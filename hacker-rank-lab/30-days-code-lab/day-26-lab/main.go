package main

import (
	"bufio"
	"fmt"
	"os"
)

// Link: https://www.hackerrank.com/challenges/30-nested-logic/problem?h_r=email&unlock_token=1ecb4ecab83acbb6f50ca8a9aa93942918f64206&utm_campaign=30_days_of_code_continuous&utm_medium=email&utm_source=daily_reminder

func main() {
	//Enter your code here. Read input from STDIN. Print output to STDOUT
	var (
		expDay, expMonth, expYear int
		actDay, actMonth, actYear int
		fine                      int = 0
	)

	scanner := bufio.NewScanner(os.Stdin)
	scanner.Scan()
	fmt.Sscanf(scanner.Text(), "%d %d %d", &actDay, &actMonth, &actYear)
	scanner.Scan()
	fmt.Sscanf(scanner.Text(), "%d %d %d", &expDay, &expMonth, &expYear)

	if actYear > expYear {
		fine = 10000
	} else if actYear == expYear {
		if actMonth > expMonth {
			fine = 500 * (actMonth - expMonth)
		} else if actMonth == expMonth && actDay > expDay {
			fine = 15 * (actDay - expDay)
		}
	} else {
		fine = 0
	}

	fmt.Println(fine)
}
