package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {
	var n int64

	phoneBook := make(map[string]string)
	scanner := bufio.NewScanner(os.Stdin)
	scanner.Scan()
	n, _ = strconv.ParseInt(scanner.Text(), 10, 64)
	for i := 0; i < int(n); i++ {
		scanner.Scan()
		rows := strings.Split(scanner.Text(), " ")
		phoneBook[rows[0]] = rows[1]
	}

	for scanner.Scan() {
		s := scanner.Text()
		if v, ok := phoneBook[s]; ok {
			fmt.Printf("%s=%s\n", s, v)
		} else {
			fmt.Println("Not found")
		}
	}
}
