package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"regexp"
	"sort"
	"strconv"
	"strings"
)

func main() {
	reader := bufio.NewReaderSize(os.Stdin, 1024*1024)

	NTemp, err := strconv.ParseInt(readLine(reader), 10, 64)
	checkError(err)
	N := int32(NTemp)

	m := []string{}
	for NItr := 0; NItr < int(N); NItr++ {
		firstNameEmailID := strings.Split(readLine(reader), " ")

		firstName := firstNameEmailID[0]

		emailID := firstNameEmailID[1]

		ex := regexp.MustCompile("[a-z]*" + firstName + "[a-z]*@gmail.com")

		matched := ex.MatchString(emailID)
		if matched {
			m = append(m, firstName)
		}
	}

	sort.Slice(m, func(i, j int) bool {
		return m[i] < m[j]
	})

	for _, v := range m {
		fmt.Println(v)
	}
}

func readLine(reader *bufio.Reader) string {
	str, _, err := reader.ReadLine()
	if err == io.EOF {
		return ""
	}

	return strings.TrimRight(string(str), "\r\n")
}

func checkError(err error) {
	if err != nil {
		panic(err)
	}
}
