package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

// Link: https://www.hackerrank.com/challenges/count-triplets-1/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=dictionaries-hashmaps

// Youtube: https://www.youtube.com/watch?v=tBFZMaWP0W8

// tips:
// 1. one iterating
// 2. two maps
// 3. test case: 1 2 1 2 4.  r=2

// Complete the countTriplets function below.
func countTriplets(arr []int64, r int64) int64 {
	leftMap := make(map[int64]int64)
	rightMap := make(map[int64]int64)

	var count int64 = 0

	for _, v := range arr {
		if _, ok := rightMap[v]; !ok {
			rightMap[v] = 1
		} else {
			rightMap[v]++
		}
	}

	for _, v := range arr {
		var c1, c3 int64 = 0, 0

		if _, ok := rightMap[v]; ok {
			rightMap[v]--
		}

		if _, ok := leftMap[v/r]; ok && v%r == 0 {
			c1 = leftMap[v/r]
		}

		if _, ok := rightMap[v*r]; ok {
			c3 = rightMap[v*r]
		}

		count = count + c1*c3

		if _, ok := leftMap[v]; !ok {
			leftMap[v] = 1
		} else {
			leftMap[v]++
		}
	}
	return count
}

func main() {

	stdin, err := os.Open("input03.txt")
	checkError(err)

	// defer stdout.Close()

	reader := bufio.NewReaderSize(stdin, 16*1024*1024)
	// writer := bufio.NewWriterSize(stdout, 16*1024*1024)

	nr := strings.Split(strings.TrimSpace(readLine(reader)), " ")

	nTemp, err := strconv.ParseInt(nr[0], 10, 64)
	checkError(err)
	n := int32(nTemp)

	r, err := strconv.ParseInt(nr[1], 10, 64)
	checkError(err)

	arrTemp := strings.Split(strings.TrimSpace(readLine(reader)), " ")

	var arr []int64

	for i := 0; i < int(n); i++ {
		arrItem, err := strconv.ParseInt(arrTemp[i], 10, 64)
		checkError(err)
		arr = append(arr, arrItem)
	}

	ans := countTriplets(arr, r)

	fmt.Println(ans)
	// fmt.Fprintf(writer, "%d\n", ans)

	// writer.Flush()
}

func readLine(reader *bufio.Reader) string {
	str, _, err := reader.ReadLine()
	if err == io.EOF {
		return ""
	}

	return strings.TrimRight(string(str), "\r\n")
}

func checkError(err error) {
	if err != nil {
		panic(err)
	}
}
