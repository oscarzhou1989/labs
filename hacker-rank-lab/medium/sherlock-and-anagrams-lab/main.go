package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"sort"
	"strconv"
	"strings"
)

// Link: https://www.hackerrank.com/challenges/sherlock-and-anagrams/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=dictionaries-hashmaps&h_r=next-challenge&h_v=zen

// Test case: abba
// d=1		target:a  check: b, b, a
//			target:b  check: b, a
//			target:b  check: a,
// d=2		target:ab	check: bb, ba
//			target:bb	check: ba
// d=3		target:abb	check: bba

// Complete the sherlockAndAnagrams function below.
func sherlockAndAnagrams(s string) int32 {
	var (
		d, i, j  int32  = 0, 0, 0
		n        int32  = int32(len(s))
		anagrams []rune = []rune(s)
		count    int32  = 0
	)

	for d = 1; d <= n-1; d++ {
		var unsortedAnagrams []string
		for j = 0; j <= n-d; j++ {
			unsortedAnagrams = append(unsortedAnagrams, string(anagrams[j:j+d]))

		}
		unsortLength := int32(len(unsortedAnagrams))
		for i = 0; i < unsortLength-1; i++ {
			subS := make(map[string]bool)
			subS[genKey(unsortedAnagrams[i], d)] = true
			for j = i + 1; j < unsortLength; j++ {
				key := genKey(unsortedAnagrams[j], d)
				if _, ok := subS[key]; ok {
					count++
				}
			}
		}
	}
	return count
}

func genKey(s string, d int32) string {
	sortedAnagrams := []rune(s)
	if d > 1 {
		sort.Slice(sortedAnagrams, func(i, j int) bool {
			return sortedAnagrams[i] < sortedAnagrams[j]
		})
	}
	return string(sortedAnagrams)
}

func main() {
	reader := bufio.NewReaderSize(os.Stdin, 1024*1024)

	stdout, err := os.Create(os.Getenv("OUTPUT_PATH"))
	checkError(err)

	defer stdout.Close()

	writer := bufio.NewWriterSize(stdout, 1024*1024)

	qTemp, err := strconv.ParseInt(readLine(reader), 10, 64)
	checkError(err)
	q := int32(qTemp)

	for qItr := 0; qItr < int(q); qItr++ {
		s := readLine(reader)

		result := sherlockAndAnagrams(s)

		fmt.Fprintf(writer, "%d\n", result)
	}

	writer.Flush()
}

func readLine(reader *bufio.Reader) string {
	str, _, err := reader.ReadLine()
	if err == io.EOF {
		return ""
	}

	return strings.TrimRight(string(str), "\r\n")
}

func checkError(err error) {
	if err != nil {
		panic(err)
	}
}
