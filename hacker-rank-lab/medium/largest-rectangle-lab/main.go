package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

// https://www.hackerrank.com/challenges/largest-rectangle/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=stacks-queues

// Youtube: https://www.youtube.com/watch?v=VNbkzsnllsU

// Complete the largestRectangle function below.
func largestRectangle(h []int32) int64 {

	posStack := NewStack()
	heightStack := NewStack()

	var (
		maxArea, area int64 = 0, 0
		i             int32 = 0
	)

	for i < int32(len(h)) {
		if heightStack.isEmpty() || heightStack.Peek().Data < h[i] {
			posStack.Push(NewNode(i))
			heightStack.Push(NewNode(h[i]))
			i++
		} else {
			height := heightStack.Pop().Data
			area = int64(height * (i - posStack.Peek().Data))
			if maxArea < area {
				maxArea = area
			}

			if !heightStack.isEmpty() {
				if h[i] > heightStack.Peek().Data {
					heightStack.Push(NewNode(h[i]))
					i++
				} else {
					posStack.Pop()
				}
			} else {
				heightStack.Push(NewNode(h[i]))
				i++
			}
		}
	}

	for !heightStack.isEmpty() {
		height := heightStack.Pop().Data
		area = int64(height * (i - posStack.Pop().Data))
		if maxArea < area {
			maxArea = area
		}
	}
	return maxArea
}

func main() {
	reader := bufio.NewReaderSize(os.Stdin, 1024*1024)

	stdout, err := os.Create(os.Getenv("OUTPUT_PATH"))
	checkError(err)

	defer stdout.Close()

	writer := bufio.NewWriterSize(stdout, 1024*1024)

	nTemp, err := strconv.ParseInt(readLine(reader), 10, 64)
	checkError(err)
	n := int32(nTemp)

	hTemp := strings.Split(readLine(reader), " ")

	var h []int32

	for i := 0; i < int(n); i++ {
		hItemTemp, err := strconv.ParseInt(hTemp[i], 10, 64)
		checkError(err)
		hItem := int32(hItemTemp)
		h = append(h, hItem)
	}

	result := largestRectangle(h)

	fmt.Fprintf(writer, "%d\n", result)

	writer.Flush()
}

func readLine(reader *bufio.Reader) string {
	str, _, err := reader.ReadLine()
	if err == io.EOF {
		return ""
	}

	return strings.TrimRight(string(str), "\r\n")
}

func checkError(err error) {
	if err != nil {
		panic(err)
	}
}
