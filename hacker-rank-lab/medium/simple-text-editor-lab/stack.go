package main

import "fmt"



type Node struct {
	Data string
	next *Node
}

func NewNode(data string) *Node {
	n := new(Node)
	n.Data = data
	n.next = nil
	return n
}

type Stack struct {
	bottom *Node
	top    *Node
}

func NewStack() *Stack {
	s := new(Stack)
	s.top = nil
	s.bottom = nil
	return s
}

func (s *Stack) Push(node *Node) {
	if s.top == s.bottom && s.top == nil {
		s.bottom = node
		s.top = node
		return
	}
	s.top.next = node
	s.top = s.top.next
	return
}

func (s *Stack) Pop() *Node {
	if s.top == s.bottom && s.top != nil {
		ret := s.top
		s.top = nil
		s.bottom = nil
		return ret
	}
	cur := s.bottom
	for cur.next != s.top {
		cur = cur.next
	}
	ret := s.top
	s.top = cur
	return ret
}

func (s *Stack) Peek() *Node {
	return s.top
}

func (s *Stack) isEmpty() bool {
	if s.top == s.bottom && s.top == nil {
		return true
	}
	return false
}

func (s *Stack) Print() {

	if s.bottom == s.top && s.top == nil {
		fmt.Println("null")
		return
	}
	cur := s.bottom
	for cur != s.top {
		fmt.Println(cur.Data)
		cur = cur.next
	}
	fmt.Println(cur.Data)
	fmt.Println("=======")
}
