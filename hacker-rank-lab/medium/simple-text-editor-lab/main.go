package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

// Link: https://www.hackerrank.com/challenges/simple-text-editor/problem?utm_campaign=challenge-recommendation&utm_medium=email&utm_source=24-hour-campaign

func main() {
	reader := bufio.NewReaderSize(os.Stdin, 1024*1024)

	stdout, err := os.Create(os.Getenv("OUTPUT_PATH"))
	checkError(err)

	defer stdout.Close()

	writer := bufio.NewWriterSize(stdout, 1024*1024)

	tTemp, err := strconv.ParseInt(readLine(reader), 10, 64)
	checkError(err)
	t := int32(tTemp)

	te := NewTextEditor()
	for tItr := 0; tItr < int(t); tItr++ {

		items := strings.Split(readLine(reader), " ")
		tyeTemp, err := strconv.ParseInt(items[0], 10, 64)
		checkError(err)

		tpe := int32(tyeTemp)

		switch tpe {
		case 1:
			te.append(items[1])
		case 2:
			valTemp, err := strconv.ParseInt(items[1], 10, 64)
			checkError(err)
			te.delete(int32(valTemp))
		case 3:
			valTemp, err := strconv.ParseInt(items[1], 10, 64)
			checkError(err)
			fmt.Fprintf(writer, "%s\n", te.print(int32(valTemp)))
		case 4:
			te.undo()
		}
	}

	writer.Flush()
}

func readLine(reader *bufio.Reader) string {
	str, _, err := reader.ReadLine()
	if err == io.EOF {
		return ""
	}

	return strings.TrimRight(string(str), "\r\n")
}

func checkError(err error) {
	if err != nil {
		panic(err)
	}
}
