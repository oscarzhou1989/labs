package main

type Operation struct {
	tpy  int32
	W    string
	k    int32
	next *Operation
	prev *Operation
}

type TextEditor struct {
	bottom *Operation
	top    *Operation
	text   string
}

func NewTextEditor() *TextEditor {
	t := new(TextEditor)
	t.bottom = nil
	t.top = nil
	t.text = ""
	return t
}

func (t *TextEditor) append(w string) {
	o := new(Operation)
	o.tpy = 1
	o.W = w
	o.k = int32(len(w))
	o.next = nil
	t.text += w

	if t.bottom == t.top && t.bottom == nil {
		t.bottom = o
		t.top = o
	}

	t.top.next = o
	o.prev = t.top
	t.top = t.top.next
}

func (t *TextEditor) delete(k int32) {
	o := new(Operation)
	o.tpy = 2
	o.W = t.text[int32(len(t.text))-k:]
	o.k = k
	o.next = nil

	l := int32(len(t.text))
	if l-k <= 0 {
		l = 0
	} else {
		l = l - k
	}
	t.text = t.text[0:l]

	if t.bottom == t.top && t.bottom == nil {
		t.bottom = o
		t.top = o
	}

	t.top.next = o
	o.prev = t.top
	t.top = t.top.next

}

func (t *TextEditor) print(k int32) string {
	// fmt.Println(t.text[k-1 : k])
	return t.text[k-1 : k]
}

func (t *TextEditor) undo() {
	if t.bottom == t.top {
		if t.top != nil {
			t.bottom = nil
			t.top = nil
		}
		t.text = ""
		return
	}

	// cur := t.bottom
	// for cur.next != t.top {
	// 	cur = cur.next
	// }

	// next := cur.next
	next := t.top
	switch next.tpy {
	case 1:
		ol := int32(len(t.text))
		dl := next.k
		if ol-dl > 0 {
			t.text = t.text[0 : ol-dl]
		} else {
			t.text = ""
		}
	case 2:
		t.text += next.W
	}

	t.top = t.top.prev
	t.top.next = nil

}
