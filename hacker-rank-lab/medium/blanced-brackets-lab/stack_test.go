package main

import "testing"

func TestStack(t *testing.T) {
	n1 := NewNode('a')
	n2 := NewNode('b')
	n3 := NewNode('c')
	n4 := NewNode('d')

	s := NewStack()
	s.Push(n1)
	s.Push(n2)
	s.Push(n3)
	s.Push(n4)
	s.Print()

	s.Pop()
	s.Print()

	s.Pop()
	s.Print()

	s.Pop()
	s.Pop()
	s.Print()
}
