package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

// Link: https://www.hackerrank.com/challenges/balanced-brackets/problem?h_l=interview&playlist_slugs%5B%5D%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D%5B%5D=stacks-queues

// Complete the isBalanced function below.
func isBalanced(s string) string {

	bracketMap := make(map[rune]rune)
	bracketMap['('] = ')'
	bracketMap['{'] = '}'
	bracketMap['['] = ']'

	runeArr := []rune(s)
	stack := NewStack()
	for _, v := range runeArr {
		n := NewNode(v)
		if _, ok := bracketMap[v]; ok {
			stack.Push(n)
		} else {
			if stack.isEmpty() {
				return "NO"
			}
			if bracketMap[stack.Peek().Data] == v {
				stack.Pop()
			} else {
				return "NO"
			}
		}
	}

	if stack.isEmpty() {
		return "YES"
	}
	return "NO"
}

func main() {
	reader := bufio.NewReaderSize(os.Stdin, 1024*1024)

	stdout, err := os.Create(os.Getenv("OUTPUT_PATH"))
	checkError(err)

	defer stdout.Close()

	writer := bufio.NewWriterSize(stdout, 1024*1024)

	tTemp, err := strconv.ParseInt(readLine(reader), 10, 64)
	checkError(err)
	t := int32(tTemp)

	for tItr := 0; tItr < int(t); tItr++ {
		s := readLine(reader)

		result := isBalanced(s)

		fmt.Fprintf(writer, "%s\n", result)
	}

	writer.Flush()
}

func readLine(reader *bufio.Reader) string {
	str, _, err := reader.ReadLine()
	if err == io.EOF {
		return ""
	}

	return strings.TrimRight(string(str), "\r\n")
}

func checkError(err error) {
	if err != nil {
		panic(err)
	}
}
