package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

// Link: https://www.hackerrank.com/challenges/insertionsort2/problem

// test case
// 3 4 7 5 6 2 1
// 3 4 7 5 6 2 1
// 3 4 5 7 6 2 1
// 3 4 5 6 7 2 1
// 2 3 4 5 6 7 1
// 1 2 3 4 5 6 7

// Complete the insertionSort2 function below.
func insertionSort2(n int32, arr []int32) {
	var (
		i, j int32 = 0, 0
	)

	for i = 1; i < n; i++ {
		pivot := arr[i]
		for j = i - 1; j >= 0; j-- {

			if arr[j] > pivot {
				arr[j+1] = arr[j]
			} else {
				arr[j+1] = pivot
				break
			}
		}
		if j == -1 {
			arr[0] = pivot
		}
		fmt.Println(strings.Trim(fmt.Sprint(arr), "[]"))
	}
}

func main() {
	reader := bufio.NewReaderSize(os.Stdin, 1024*1024)

	nTemp, err := strconv.ParseInt(readLine(reader), 10, 64)
	checkError(err)
	n := int32(nTemp)

	arrTemp := strings.Split(readLine(reader), " ")

	var arr []int32

	for i := 0; i < int(n); i++ {
		arrItemTemp, err := strconv.ParseInt(arrTemp[i], 10, 64)
		checkError(err)
		arrItem := int32(arrItemTemp)
		arr = append(arr, arrItem)
	}

	insertionSort2(n, arr)
}

func readLine(reader *bufio.Reader) string {
	str, _, err := reader.ReadLine()
	if err == io.EOF {
		return ""
	}

	return strings.TrimRight(string(str), "\r\n")
}

func checkError(err error) {
	if err != nil {
		panic(err)
	}
}
