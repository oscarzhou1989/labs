package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

// Link: https://www.hackerrank.com/challenges/migratory-birds/problem?utm_campaign=challenge-recommendation&utm_medium=email&utm_source=24-hour-campaign

// Complete the migratoryBirds function below.
func migratoryBirds(arr []int32) int32 {

	hashMap := make(map[int32]int32)
	var (
		frq int32 = 0
		min int32 = 0
	)
	for _, v := range arr {
		if _, ok := hashMap[v]; !ok {
			hashMap[v] = 1
		} else {
			hashMap[v]++
			if frq < hashMap[v] {
				frq = hashMap[v]
				min = v
			} else if frq == hashMap[v] {
				if min > v {
					min = v
				}
			}
		}
	}
	return min
}

func main() {

	stdin, err := os.Open("input04.txt")
	checkError(err)

	defer stdin.Close()

	reader := bufio.NewReaderSize(stdin, 16*1024*1024)
	// writer := bufio.NewWriterSize(stdout, 16*1024*1024)

	arrCount, err := strconv.ParseInt(strings.TrimSpace(readLine(reader)), 10, 64)
	checkError(err)

	arrTemp := strings.Split(strings.TrimSpace(readLine(reader)), " ")

	var arr []int32

	for i := 0; i < int(arrCount); i++ {
		arrItemTemp, err := strconv.ParseInt(arrTemp[i], 10, 64)
		checkError(err)
		arrItem := int32(arrItemTemp)
		arr = append(arr, arrItem)
	}

	result := migratoryBirds(arr)

	fmt.Println(result)
	// fmt.Fprintf(writer, "%d\n", result)

	// writer.Flush()
}

func readLine(reader *bufio.Reader) string {
	str, _, err := reader.ReadLine()
	if err == io.EOF {
		return ""
	}

	return strings.TrimRight(string(str), "\r\n")
}

func checkError(err error) {
	if err != nil {
		panic(err)
	}
}
