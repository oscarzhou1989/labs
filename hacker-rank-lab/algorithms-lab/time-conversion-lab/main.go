package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strings"
)

// Link: https://www.hackerrank.com/challenges/time-conversion/problem?utm_campaign=challenge-recommendation&utm_medium=email&utm_source=24-hour-campaign

/*
 * Complete the timeConversion function below.
 */
func timeConversion(s string) string {
	/*
	 * Write your code here.
	 */

	var (
		hh, mm, ss int
		p          string
	)
	fmt.Sscanf(s, "%02d:%02d:%02d%s", &hh, &mm, &ss, &p)

	// test case 1: 12:xx:xxAM -> 00:xx:xx
	// test case 2: 12:xx:xxPM -> 12:xx:xx
	// test case 3: 05:xx:xxPM -> 17:xx:xx
	if p == "AM" && hh == 12 {
		hh = 0
	} else if p == "PM" && hh == 12 {
		hh = 12
	} else if p == "PM" {
		hh += 12
	}
	return fmt.Sprintf("%02d:%02d:%02d", hh, mm, ss)
}

func main() {
	reader := bufio.NewReaderSize(os.Stdin, 1024*1024)

	outputFile, err := os.Create(os.Getenv("OUTPUT_PATH"))
	checkError(err)

	defer outputFile.Close()

	writer := bufio.NewWriterSize(outputFile, 1024*1024)

	s := readLine(reader)

	result := timeConversion(s)

	fmt.Fprintf(writer, "%s\n", result)

	writer.Flush()
}

func readLine(reader *bufio.Reader) string {
	str, _, err := reader.ReadLine()
	if err == io.EOF {
		return ""
	}

	return strings.TrimRight(string(str), "\r\n")
}

func checkError(err error) {
	if err != nil {
		panic(err)
	}
}
