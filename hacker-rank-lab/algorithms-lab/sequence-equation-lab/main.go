package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

// https://www.hackerrank.com/challenges/permutation-equation/problem

// case: 4 3 5 1 2
// p(1)=4     pp(4)=1
// p(2)=3     pp(3)=2
// p(3)=5     pp(5)=3
// p(4)=1     pp(1)=4
// p(5)=2     pp(2)=5

// x=1   y=pp(pp(1))=1
// x=2   y=pp(pp(2))=3
// x=3   y=pp(pp(3))=5
// x=4   y=pp(pp(4))=4
// x=5   y=pp(pp(5))=2

// Complete the permutationEquation function below.
func permutationEquation(p []int32) []int32 {

	ppConvert := make(map[int32]int32)
	for i, v := range p {
		ppConvert[v] = int32(i + 1)
	}

	retArr := make([]int32, len(p))
	for i := range p {
		retArr[i] = ppConvert[ppConvert[int32(i+1)]]
	}
	return retArr
}

func main() {
	reader := bufio.NewReaderSize(os.Stdin, 1024*1024)

	stdout, err := os.Create(os.Getenv("OUTPUT_PATH"))
	checkError(err)

	defer stdout.Close()

	writer := bufio.NewWriterSize(stdout, 1024*1024)

	nTemp, err := strconv.ParseInt(readLine(reader), 10, 64)
	checkError(err)
	n := int32(nTemp)

	pTemp := strings.Split(readLine(reader), " ")

	var p []int32

	for i := 0; i < int(n); i++ {
		pItemTemp, err := strconv.ParseInt(pTemp[i], 10, 64)
		checkError(err)
		pItem := int32(pItemTemp)
		p = append(p, pItem)
	}

	result := permutationEquation(p)

	for i, resultItem := range result {
		fmt.Fprintf(writer, "%d", resultItem)

		if i != len(result)-1 {
			fmt.Fprintf(writer, "\n")
		}
	}

	fmt.Fprintf(writer, "\n")

	writer.Flush()
}

func readLine(reader *bufio.Reader) string {
	str, _, err := reader.ReadLine()
	if err == io.EOF {
		return ""
	}

	return strings.TrimRight(string(str), "\r\n")
}

func checkError(err error) {
	if err != nil {
		panic(err)
	}
}
