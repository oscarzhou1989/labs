package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

type Node struct {
	rank  int32
	score int32
	next  *Node
}

type LinkedList struct {
	head *Node
}

func (e *LinkedList) Add(score int32) {
	newNode := new(Node)
	newNode.score = score
	newNode.next = nil

	if e.head == nil {
		e.head = newNode
	} else {
		var prevNode *Node = nil
		curNode := e.head

		for curNode != nil {
			if curNode.score > newNode.score {
				if curNode.next != nil {
					prevNode = curNode
					curNode = curNode.next
				} else {
					curNode.next = newNode
					break
				}
			} else {
				newNode.next = curNode
				if prevNode != nil {
					prevNode.next = newNode
				} else {
					e.head = newNode
				}
				break
			}

		}
	}
}

func (e *LinkedList) Find(score int32) int32 {
	curNode := e.head
	for curNode != nil {
		if curNode.score == score {
			return curNode.rank
		}

		if curNode.next != nil {
			curNode = curNode.next
		}
	}
	return -1
}

func (e *LinkedList) RefreshRank() {
	if e.head != nil {
		var (
			rank     int32 = 1
			prevNode *Node = e.head
			curNode  *Node = e.head
		)
		for curNode != nil {
			curNode.rank = rank
			prevNode = curNode
			curNode = curNode.next
			if curNode != nil {
				if prevNode.score != curNode.score {
					rank++
				}
			}

		}
	}
}

func (e *LinkedList) Insert(score int32) int32 {
	newNode := new(Node)
	newNode.score = score
	newNode.next = nil

	var rank int32 = 1
	if e.head == nil {
		newNode.rank = rank
		e.head = newNode
	} else {
		var prevNode *Node = nil
		curNode := e.head
		for curNode != nil {
			if curNode.score > newNode.score {
				if curNode.next != nil {
					prevNode = curNode
					curNode = curNode.next
					if curNode != nil {
						if prevNode.score != curNode.score {
							rank++
						}
					}
				} else {
					curNode.next = newNode
					rank++
					return rank
				}
			} else {
				newNode.next = curNode
				if prevNode != nil {
					prevNode.next = newNode
				} else {
					e.head = newNode
				}
				return rank
			}
		}
	}
	return -1
}

func binarySearch(scores []int32, target int32) (pos int) {
	low := 0
	high := len(scores) - 1

	for low <= high {
		mid := (low + high) / 2
		if scores[mid] == target {
			pos = mid
			return
		}

		if scores[mid] < target && target < scores[mid-1] {
			pos = mid
			return
		} else if scores[mid] > target && target > scores[mid+1] {
			pos = mid + 1
			return
		} else if scores[mid] < target {
			high = mid - 1
		} else if scores[mid] > target {
			low = mid + 1
		}
	}
	return
}

// Link: https://www.hackerrank.com/challenges/climbing-the-leaderboard/problem

// binarySearch to find the next index

// can't pass test case 6,8,9
// very wired

// test case 6,8 can get true result
// didn't unlock test case 9

// Youtube: https://www.youtube.com/watch?v=CAyXHTqBIBU&list=PLSIpQf0NbcCltzNFrOJkQ4J4AAjW3TSmA
// Complete the climbingLeaderboard function below.
func climbingLeaderboard(scores []int32, alice []int32) []int32 {
	var (
		i    int = 0
		n    int = len(scores)
		rank     = make([]int32, len(scores))
	)

	rank[0] = 1
	// Time complexity: O(n)
	for i = 1; i < n; i++ {
		if scores[i] == scores[i-1] {
			rank[i] = rank[i-1]
		} else {
			rank[i] = rank[i-1] + 1
		}
	}

	lastIndex := n - 1
	retArr := make([]int32, len(alice))
	// Time complexity: O(n)
	for i, v := range alice {
		if v > scores[0] {
			retArr[i] = 1
		} else if v < scores[lastIndex] {
			retArr[i] = rank[lastIndex] + 1
		} else {
			pos := binarySearch(scores, v)
			retArr[i] = rank[pos]
		}

		// fmt.Printf("i=%d, v=%d\n", i, v)
		// fmt.Println(retArr[i])
	}

	return retArr
}

func main() {
	// reader := bufio.NewReaderSize(os.Stdin, 1024*1024)

	stdin, err := os.Open("input01.txt")
	checkError(err)

	defer stdin.Close()

	reader := bufio.NewReaderSize(stdin, 1024*1024*10)
	// writer := bufio.NewWriterSize(stdout, 1024*1024)

	scoresCount, err := strconv.ParseInt(readLine(reader), 10, 64)
	checkError(err)

	scoresTemp := strings.Split(readLine(reader), " ")

	var scores []int32

	for i := 0; i < int(scoresCount); i++ {
		scoresItemTemp, err := strconv.ParseInt(scoresTemp[i], 10, 64)
		// fmt.Println("===", i)
		// fmt.Println("+++", scoresTemp[i])
		checkError(err)
		scoresItem := int32(scoresItemTemp)
		scores = append(scores, scoresItem)
	}

	aliceCount, err := strconv.ParseInt(readLine(reader), 10, 64)
	checkError(err)

	aliceTemp := strings.Split(readLine(reader), " ")

	var alice []int32

	for i := 0; i < int(aliceCount); i++ {
		aliceItemTemp, err := strconv.ParseInt(aliceTemp[i], 10, 64)
		checkError(err)
		aliceItem := int32(aliceItemTemp)
		alice = append(alice, aliceItem)
	}

	result := climbingLeaderboard(scores, alice)

	for _, resultItem := range result {
		// fmt.Fprintf(writer, "%d", resultItem)

		fmt.Println(resultItem)
		// if i != len(result)-1 {
		// 	fmt.Fprintf(writer, "\n")
		// }
	}

	// fmt.Fprintf(writer, "\n")

	// writer.Flush()
}

func readLine(reader *bufio.Reader) string {
	str, _, err := reader.ReadLine()
	if err == io.EOF {
		return ""
	}

	return strings.TrimRight(string(str), "\r\n")
}

func checkError(err error) {
	if err != nil {
		panic(err)
	}
}
