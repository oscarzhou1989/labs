package main

import (
    "bufio"
    "fmt"
    "io"
    "os"
    "strconv"
    "strings"
)

// Link: https://www.hackerrank.com/challenges/insertionsort2/problem

// Complete the insertionSort2 function below.
func insertionSort2(n int32, arr []int32) {
    var (
        i, j int32 = 0,0
    )

    for i=1; i<n; i++{
        for j=0;j<i;j++{
            if arr[j]>arr[i]{
                arr[j], arr[i] = arr[i],arr[j]
            }
        }

        for _,v:=range arr{
            fmt.Printf("%d ", v)
        }
        fmt.Println();
    }
}

func main() {
    reader := bufio.NewReaderSize(os.Stdin, 1024 * 1024)

    nTemp, err := strconv.ParseInt(readLine(reader), 10, 64)
    checkError(err)
    n := int32(nTemp)

    arrTemp := strings.Split(readLine(reader), " ")

    var arr []int32

    for i := 0; i < int(n); i++ {
        arrItemTemp, err := strconv.ParseInt(arrTemp[i], 10, 64)
        checkError(err)
        arrItem := int32(arrItemTemp)
        arr = append(arr, arrItem)
    }

    insertionSort2(n, arr)
}

func readLine(reader *bufio.Reader) string {
    str, _, err := reader.ReadLine()
    if err == io.EOF {
        return ""
    }

    return strings.TrimRight(string(str), "\r\n")
}

func checkError(err error) {
    if err != nil {
        panic(err)
    }
}
