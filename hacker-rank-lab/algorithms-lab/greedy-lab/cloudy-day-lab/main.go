package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

// Link: https://www.hackerrank.com/challenges/cloudy-day/problem?h_r=next-challenge&h_v=zen

// ---- case -----//
// input:

// 3
// 800 200 900
// 10 50 70
// 3
// 20 50 90
// 10 15 5

// output:
// 1700
// ---- End case -----//

// ---- case5 -----//
// 1
// 710968056
// 648957198
// 6
// 537860869 932700775 322731914 335848765 570747597 145761817
// 867458069 840476761 699140591 251654375 892806344 860058735

// 0
// ---- End case5 -----//

// Complete the maximumPeople function below.
func maximumPeople(p []int64, x []int64, y []int64, r []int64) int64 {
	// Return the maximum number of people that will be in a sunny town after removing exactly one cloud.

	// p is population
	// x is location of the town
	// y is location of cloud
	// r is range of each cloud

	// sorting the town by population ascending
	townArray := make(map[int64][]int64)
	for i, v := range p {
		townArray[v] = append(townArray[v], int64(i))
	}

	//
	var keys []int64
	for key := range townArray {
		keys = append(keys, key)
	}

	keys = quickSort(keys)

	var (
		totalPopulation int64
		removed         bool = false
	)
	for i := len(keys) - 1; i >= 0; i-- {
		// start from the town with maximum population first
		for _, townNo := range townArray[keys[i]] {
			population := p[townNo]

			coverNum := coverCloudNum(x[townNo], y, r)
			if coverNum > 0 {
				if coverNum == 1 {
					if !removed {
						removed = true
						removeCloud(x[townNo], &y, &r)
						totalPopulation += population
					}
				}
			} else {
				totalPopulation += population
			}
		}
	}
	return totalPopulation
}

func quickSort(arr []int64) []int64 {
	if len(arr) < 2 {
		return arr
	}

	pivot := arr[len(arr)/2]
	var left, right []int64
	for i, v := range arr {
		if i == len(arr)/2 {
			continue
		}

		if v < pivot {
			left = append(left, v)
		} else {
			right = append(right, v)
		}
	}

	var retArr []int64
	retArr = append(retArr, quickSort(left)...)
	retArr = append(retArr, pivot)
	retArr = append(retArr, quickSort(right)...)
	return retArr

}

func coverCloudNum(loc int64, y []int64, r []int64) int {
	count := 0
	for i, cloudLoc := range y {
		if cloudLoc-r[i] == loc ||
			cloudLoc+r[i] == loc ||
			(cloudLoc-r[i] < loc && loc < cloudLoc+r[i]) {
			count++
		}
	}
	return count
}

func removeCloud(loc int64, y *[]int64, r *[]int64) {
	for i, cloudLoc := range *y {
		if cloudLoc-(*r)[i] == loc ||
			cloudLoc+(*r)[i] == loc ||
			(cloudLoc-(*r)[i] < loc && loc < cloudLoc+(*r)[i]) {
			(*y)[i] = 0
			(*r)[i] = 0
		}
	}
}

func main() {
	reader := bufio.NewReaderSize(os.Stdin, 1024*1024)

	stdout, err := os.Create(os.Getenv("OUTPUT_PATH"))
	checkError(err)

	defer stdout.Close()

	writer := bufio.NewWriterSize(stdout, 1024*1024)

	nTemp, err := strconv.ParseInt(readLine(reader), 10, 64)
	checkError(err)
	n := int32(nTemp)

	pTemp := strings.Split(readLine(reader), " ")

	var p []int64

	for i := 0; i < int(n); i++ {
		pItem, err := strconv.ParseInt(pTemp[i], 10, 64)
		checkError(err)
		p = append(p, pItem)
	}

	xTemp := strings.Split(readLine(reader), " ")

	var x []int64

	for i := 0; i < int(n); i++ {
		xItem, err := strconv.ParseInt(xTemp[i], 10, 64)
		checkError(err)
		x = append(x, xItem)
	}

	mTemp, err := strconv.ParseInt(readLine(reader), 10, 64)
	checkError(err)
	m := int32(mTemp)

	yTemp := strings.Split(readLine(reader), " ")

	var y []int64

	for i := 0; i < int(m); i++ {
		yItem, err := strconv.ParseInt(yTemp[i], 10, 64)
		checkError(err)
		y = append(y, yItem)
	}

	rTemp := strings.Split(readLine(reader), " ")

	var r []int64

	for i := 0; i < int(m); i++ {
		rItem, err := strconv.ParseInt(rTemp[i], 10, 64)
		checkError(err)
		r = append(r, rItem)
	}

	result := maximumPeople(p, x, y, r)

	fmt.Fprintf(writer, "%d\n", result)

	writer.Flush()
}

func readLine(reader *bufio.Reader) string {
	str, _, err := reader.ReadLine()
	if err == io.EOF {
		return ""
	}

	return strings.TrimRight(string(str), "\r\n")
}

func checkError(err error) {
	if err != nil {
		panic(err)
	}
}
