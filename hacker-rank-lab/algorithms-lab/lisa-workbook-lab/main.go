package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

// Link: https://www.hackerrank.com/challenges/lisa-workbook/problem?utm_campaign=challenge-recommendation&utm_medium=email&utm_source=24-hour-campaign

// start: the first question number of the current page
// end: the last question number of the current page

// Complete the workbook function below.
func workbook(n int32, k int32, arr []int32) int32 {

	var (
		page  int32 = 1
		count int32 = 0
		i     int32 = 0
	)

	for _, chapter := range arr {
		// test case chapter=4, k=3
		x := chapter / k // x=1
		j := chapter % k // j=1
		if j != 0 {
			x++
		}

		for i = 0; i < x; i++ {
			start := (i * k) + 1 // start = 1
			end := (i + 1) * k   // end = 3
			if i == x-1 && j != 0 {
				start = (i * k) + 1 // start =4
				end = (i * k) + j   // end=4
			}

			if start <= page && page <= end {
				count++
			}
			page++
		}
	}
	return count
}

func main() {
	reader := bufio.NewReaderSize(os.Stdin, 1024*1024)

	stdout, err := os.Create(os.Getenv("OUTPUT_PATH"))
	checkError(err)

	defer stdout.Close()

	writer := bufio.NewWriterSize(stdout, 1024*1024)

	nk := strings.Split(readLine(reader), " ")

	nTemp, err := strconv.ParseInt(nk[0], 10, 64)
	checkError(err)
	n := int32(nTemp)

	kTemp, err := strconv.ParseInt(nk[1], 10, 64)
	checkError(err)
	k := int32(kTemp)

	arrTemp := strings.Split(readLine(reader), " ")

	var arr []int32

	for i := 0; i < int(n); i++ {
		arrItemTemp, err := strconv.ParseInt(arrTemp[i], 10, 64)
		checkError(err)
		arrItem := int32(arrItemTemp)
		arr = append(arr, arrItem)
	}

	result := workbook(n, k, arr)

	fmt.Fprintf(writer, "%d\n", result)

	writer.Flush()
}

func readLine(reader *bufio.Reader) string {
	str, _, err := reader.ReadLine()
	if err == io.EOF {
		return ""
	}

	return strings.TrimRight(string(str), "\r\n")
}

func checkError(err error) {
	if err != nil {
		panic(err)
	}
}
