package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

// Link: https://www.hackerrank.com/challenges/dynamic-array/problem

/*
 * Complete the 'dynamicArray' function below.
 *
 * The function is expected to return an INTEGER_ARRAY.
 * The function accepts following parameters:
 *  1. INTEGER n
 *  2. 2D_INTEGER_ARRAY queries
 */

func dynamicArray(n int32, queries [][]int32) []int32 {
	// Write your code here
	var lastAnswer int32 = 0
	seq := make([][]int32, n)

	var retArr []int32
	for _, row := range queries {
		index := (row[1] ^ lastAnswer) % n
		if row[0] == 1 {
			seq[index] = append(seq[index], row[2])
		} else if row[0] == 2 {
			var size int32 = int32(len(seq[index]))
			lastAnswer = seq[index][row[2]%size]
			retArr = append(retArr, lastAnswer)
		}
	}
	return retArr
}

func main() {

	stdin, err := os.Open("input01.txt")
	checkError(err)

	// defer stdin.Close()

	reader := bufio.NewReaderSize(stdin, 16*1024*1024)
	// writer := bufio.NewWriterSize(stdout, 16*1024*1024)

	firstMultipleInput := strings.Split(strings.TrimSpace(readLine(reader)), " ")

	nTemp, err := strconv.ParseInt(firstMultipleInput[0], 10, 64)
	checkError(err)
	n := int32(nTemp)

	qTemp, err := strconv.ParseInt(firstMultipleInput[1], 10, 64)
	checkError(err)
	q := int32(qTemp)

	var queries [][]int32
	for i := 0; i < int(q); i++ {
		queriesRowTemp := strings.Split(strings.TrimRight(readLine(reader), " \t\r\n"), " ")

		var queriesRow []int32
		for _, queriesRowItem := range queriesRowTemp {
			queriesItemTemp, err := strconv.ParseInt(queriesRowItem, 10, 64)
			checkError(err)
			queriesItem := int32(queriesItemTemp)
			queriesRow = append(queriesRow, queriesItem)
		}

		if len(queriesRow) != 3 {
			panic("Bad input")
		}

		queries = append(queries, queriesRow)
	}

	result := dynamicArray(n, queries)

	for i, resultItem := range result {
		// fmt.Fprintf(writer, "%d", resultItem)
		if i == 1 {
			return
		}
		fmt.Printf("%d\n", resultItem)
		// if i != len(result)-1 {
		// 	fmt.Fprintf(writer, "\n")
		// }
	}

	// fmt.Fprintf(writer, "\n")

	// writer.Flush()
}

func readLine(reader *bufio.Reader) string {
	str, _, err := reader.ReadLine()
	if err == io.EOF {
		return ""
	}

	return strings.TrimRight(string(str), "\r\n")
}

func checkError(err error) {
	if err != nil {
		panic(err)
	}
}
