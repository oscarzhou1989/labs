package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

// Link: https://www.hackerrank.com/challenges/new-year-chaos/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=arrays

// This problem tests the count of swap

// Test case

// 5 1 2 3 7 8 6 4
// Too chaotic

// 1 2 5 3 7 8 6 4
// 7

// Complete the minimumBribes function below.
func minimumBribes(q []int32) {
	var (
		n     int32 = int32(len(q))
		i     int32 = 0
		count int32 = 0
	)

	for i = 0; i < n; i++ {
		actualNumber := i + 1

		// Has this person moved more than two position forward?
		if q[i]-2 > actualNumber {
			fmt.Println("Too chaotic")
			return
		}
	}

	// 1 2 5 3 7 8 6 4
	// 1 2 3 5 7 6 4 8   3
	// 1 2 3 5 6 4 7 8   2
	// 1 2 3 5 4 6 7 8   1
	// 1 2 3 4 5 6 7 8   1

	i = 0
	for i < n-1 {
		for j := i; j < n-1; j++ {
			if q[j] > q[j+1] {
				q[j], q[j+1] = q[j+1], q[j]
				count++
			} else if q[j] == i+1 {
				i = q[j]
			}
		}
	}
	fmt.Println(count)

}

func main() {
	reader := bufio.NewReaderSize(os.Stdin, 1024*1024)

	tTemp, err := strconv.ParseInt(readLine(reader), 10, 64)
	checkError(err)
	t := int32(tTemp)

	for tItr := 0; tItr < int(t); tItr++ {
		nTemp, err := strconv.ParseInt(readLine(reader), 10, 64)
		checkError(err)
		n := int32(nTemp)

		qTemp := strings.Split(readLine(reader), " ")

		var q []int32

		for i := 0; i < int(n); i++ {
			qItemTemp, err := strconv.ParseInt(qTemp[i], 10, 64)
			checkError(err)
			qItem := int32(qItemTemp)
			q = append(q, qItem)
		}

		minimumBribes(q)
	}
}

func readLine(reader *bufio.Reader) string {
	str, _, err := reader.ReadLine()
	if err == io.EOF {
		return ""
	}

	return strings.TrimRight(string(str), "\r\n")
}

func checkError(err error) {
	if err != nil {
		panic(err)
	}
}
