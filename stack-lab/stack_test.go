package stack

import "testing"

var s *Stack

func TestPush(t *testing.T) {
	s = NewStack()
	s.Push(1)
	s.Push(3)
	s.Push(4)
	if size := len(s.Items); size != 3 {
		t.Errorf("wrong count: want: 3, got: %d", size)
	}
	s.Print()
}

func TestPop(t *testing.T) {
	s.Pop()
	if size := len(s.Items); size != 2 {
		t.Errorf("wrong count: want: 2, got: %d", size)
	}

	s.Print()

	s.Pop()
	s.Pop()
	if size := len(s.Items); size != 0 {
		t.Errorf("wrong count: want: 0, got: %d", size)
	}
	s.Print()
}
