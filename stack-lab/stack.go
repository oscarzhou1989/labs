package stack

import (
	"fmt"
	"sync"
)

type Stack struct {
	Items []interface{}
	lock  sync.RWMutex
}

func NewStack() *Stack {
	s := Stack{
		Items: []interface{}{},
	}
	return &s
}

func (s *Stack) Push(item interface{}) {
	s.lock.Lock()
	s.Items = append(s.Items, item)
	s.lock.Unlock()
}

func (s *Stack) Pop() interface{} {
	length := len(s.Items)
	if length != 0 {
		s.lock.Lock()
		item := s.Items[length-1]
		s.Items = s.Items[:length-1]
		s.lock.Unlock()
		return item
	}
	return nil
}

func (s *Stack) Print() {
	fmt.Println("==========")
	for _, v := range s.Items {
		fmt.Println(v)
	}
	fmt.Println("==========")
}
