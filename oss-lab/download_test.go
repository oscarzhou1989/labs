package oss

import "testing"

func TestDownloadImage(t *testing.T) {

	downloadURL := "https://timingniao-web.oss-cn-hangzhou.aliyuncs.com/banner-auckland-8-days%20(1)-1565783846230619686.jpg"

	if err := DownloadImage(downloadURL); err != nil {
		t.Error(err)
	}

}
