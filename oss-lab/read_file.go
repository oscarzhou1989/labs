package oss

import (
	"bufio"
	"encoding/base64"
	"fmt"
	"os"
	"path/filepath"
)

func ReadFile2Base64(path string) (string, error) {
	fileName, err := FindImagePath(path)
	if err != nil {
		return "", err
	}

	ext := filepath.Ext(fileName)[1:]
	d := path + "/" + fileName
	file, err := os.Open(d)
	if err != nil {
		return "", err
	}
	defer file.Close()

	fileInfo, err := file.Stat()
	if err != nil {
		return "", err
	}

	bytes := make([]byte, fileInfo.Size())
	buffer := bufio.NewReader(file)
	_, err = buffer.Read(bytes)
	if err != nil {
		return "", err
	}

	return fmt.Sprintf("data:image/%s;base64,%s", ext,
		base64.StdEncoding.EncodeToString(bytes)), nil
}
