package oss

import (
	"fmt"
	"io/ioutil"
	"strings"
)

func FindImagePath(path string) (string, error) {

	fmt.Println("path= ", path)

	files, err := ioutil.ReadDir(path)
	if err != nil {
		return "", err
	}

	for _, file := range files {
		fmt.Println("file name= ", file.Name())
		if strings.Contains(file.Name(), "poster") {
			return file.Name(), nil
		}
	}
	return "", nil
}
