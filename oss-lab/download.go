package oss

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"path/filepath"
)

func DownloadImage(imageURL string) error {

	// fmt.Println("fileUrl= ", imageURL)
	// fileUrl, err := url.Parse(imageURL)
	// if err != nil {
	// 	return err
	// }

	// fmt.Println("fileUrl= ", fileUrl)

	// segments := strings.Split(fileUrl.Path, "/")
	// fileName := segments[len(segments)-1]
	ext := filepath.Ext(imageURL)
	fmt.Println("ext= ", ext)

	resp, err := http.Get(imageURL)
	if err != nil {
		return err
	}

	defer resp.Body.Close()

	file, err := os.Create("poster/poster" + ext)
	if err != nil {
		return err
	}
	defer file.Close()

	_, err = io.Copy(file, resp.Body)
	if err != nil {
		return err
	}
	return nil
}
