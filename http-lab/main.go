package main

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"time"
)

func main() {
	// Hello world, the web server

	helloHandler := func(w http.ResponseWriter, req *http.Request) {
		// io.WriteString(w, "Hello, world!\n")

		w.WriteHeader(500)
		io.WriteString(w, "no found")

	}

	offlineHandler := func(w http.ResponseWriter, req *http.Request) {

		if isOfflineTime() {
			io.WriteString(w, "offline time")
		} else {
			io.WriteString(w, "online time")
		}
	}

	http.HandleFunc("/hello", helloHandler)
	http.HandleFunc("/offline", offlineHandler)

	log.Fatal(http.ListenAndServe(":8080", nil))
}

// isOfflineTime from
func isOfflineTime() bool {
	var offlineFrom, offlineTo time.Time

	now := time.Now()
	// shangHaiTimeZone := "Asia/Shanghai"
	aucklandTimeZone := "Pacific/Auckland"
	loc, _ := time.LoadLocation(aucklandTimeZone)
	offlineFrom = time.Date(now.Year(), now.Month(), now.Day(), 16, 51, 0, 0, loc)

	loc, _ = time.LoadLocation(aucklandTimeZone)

	// nextDay := now.Add(time.Hour * 24)
	// offlineTo = time.Date(nextDay.Year(), nextDay.Month(), nextDay.Day(), 17, 0, 0, 0, loc)
	offlineTo = time.Date(now.Year(), now.Month(), now.Day(), 17, 01, 0, 0, loc)

	fmt.Println("now=", now)
	fmt.Println("from=", offlineFrom)
	fmt.Println("to=", offlineTo)
	if now.After(offlineFrom) && now.Before(offlineTo) {
		return true
	}
	return false
}
