package queue

import (
	"fmt"
	"sync"
)

type Queue struct {
	Items []interface{}
	lock  sync.RWMutex
}

func NewQueue() *Queue {
	q := Queue{
		Items: []interface{}{},
	}

	return &q
}

func (q *Queue) Enqueue(item interface{}) {
	q.lock.Lock()
	q.Items = append(q.Items, item)
	q.lock.Unlock()
}

func (q *Queue) Dequeue() interface{} {
	length := len(q.Items)
	if length != 0 {
		q.lock.Lock()
		item := q.Items[0]
		q.Items = q.Items[1:length]
		q.lock.Unlock()
		return item
	}
	return nil
}




func (q *Queue) Print() {
	fmt.Println("======")
	for _, v := range q.Items {
		fmt.Println(v)
	}
	fmt.Println("======")
}
