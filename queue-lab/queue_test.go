package queue

import "testing"

var q *Queue

func TestEnqueue(t *testing.T) {
	q = NewQueue()

	q.Enqueue(1)
	q.Enqueue(10)
	q.Enqueue(39)
	if size := len(q.Items); size != 3 {
		t.Errorf("count wrong, want 3, got %d", size)
	}
	q.Print()
}

func TestDequeue(t *testing.T) {
	q.Dequeue()
	if size := len(q.Items); size != 2 {
		t.Errorf("count wrong, want 2, got %d", size)
	}

	q.Print()

	q.Dequeue()
	q.Dequeue()
	if size := len(q.Items); size != 0 {
		t.Errorf("count wrong, want 0, got %d", size)
	}

	q.Print()
}
