package main

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
)

type Animal struct {
	Genre       string
	Environment string
}

var animals []Animal

func init() {
	animals = append(animals, Animal{
		"dog", "land",
	})
	animals = append(animals, Animal{
		"goldfish", "water",
	})
	animals = append(animals, Animal{
		"hawk", "sky",
	})
}

func main() {
	fmt.Println("1")
	engine := gin.New()

	fmt.Println("2")
	engine.Handle(http.MethodGet, "/animals", Get)
	engine.Handle(http.MethodPost, "/animals", Post)
	engine.Handle(http.MethodPost, "/animals/:app_id", PostByID)

	fmt.Println("3")
	engine.Run(":7000")
}

func Get(ctx *gin.Context) {
	if len(animals) == 0 {
		ctx.JSON(http.StatusNoContent, nil)
	}

	ctx.JSON(http.StatusOK, animals)
	return
}

func Post(ctx *gin.Context) {
	var animal Animal
	if err := ctx.Bind(&animal); err != nil {
		ctx.Err()
		return
	}

	animals = append(animals, animal)
	ctx.JSON(http.StatusOK, animals)
	return
}

func PostByID(ctx *gin.Context) {
	fmt.Println("===", ctx.Param("app_id"))
}
