1. You manually launch a NAT AMI in a public subnet. The network is properly configured. Security groups and
network access control lists are property configured. Instances in a private subnet can access the NAT. The
NAT can access the Internet. However, private instances cannot access the Internet. What additional step is
required to allow access from the private instances?  


A.Enable Source/Destination Check on the private Instances.
B.Enable Source/Destination Check on the NAT instance.
C.Disable Source/Destination Check on the private instances.
D.Disable Source/Destination Check on the NAT instance.


Disabling Source/Destination Checks.
Each EC2 instance performs source/destination checks by default. This means that the instance must be the
source or destination of any traffic it sends or receives. However, a NAT instance must be able to send and
receive traffic when the source or destination is not itself. Therefore, you must disable source/destination
checks on the NAT instance. You can disable the SrcDestCheck attribute for a NAT instance that’s either
running or stopped using the console or the command line.
http://docs.aws.amazon.com/AmazonVPC/latest/UserGuide/VPC_NAT_Instance.html  


---

2. A legacy application needs to interact with local storage using iSCSI. A team needs to design a reliable storage solution to provision all new storage on AWS. Which storage solution meets the legacy application requirements?

A. AWS Snowball storage for the legacy application until the application can be re-architected.  
B. AWS Storage Gateway in cached mode for the legacy application storage to write data to Amazon S3.  
C. AWS Storage Gateway in stored mode for the legacy application storage to write data to Amazon S3.  
D. An Amazon S3 volume mounted on the legacy application server locally using the File Gateway service.  


The answer is C, but I doubt it, I think it's B. [Reference](https://www.briefmenow.org/amazon/aws-certified-solutions-architect-associate-2018-legacy-application-needs-to-interact-with-local-storage-using-iscsi-team-needs-to-design-reliable-storage-solution-to-provision-all-new-stora/)




---

3. A company is launching an application that it expects to be very popular. The company needs a database that can scale with the rest of the application. The schema will change frequently. The application cannot afford any downtime for database changes.
Which AWS service allows the company to achieve these objectives?

A. Amazon Redshift  
B. Amazon DynamoDB  
C. Amazon RDS MySQL  
D. Amazon Aurora  

---

The answer is A, but I perfer B  

