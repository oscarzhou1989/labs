1. [EMR + Spot Instance](https://docs.aws.amazon.com/emr/latest/ManagementGuide/emr-plan-instances-guidelines.html#emr-plan-spot-instances)  
2. ??? [Public IP address managed in Instance](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-instance-metadata.html#instancedata-data-retrieval)  
3. 

6. ???
8. [VPC peering](https://docs.aws.amazon.com/vpc/latest/peering/invalid-peering-configurations.html#edge-to-edge-vgw)  
10. ??? [Flow log](https://docs.aws.amazon.com/vpc/latest/userguide/flow-logs.html)  
11. [DynamoDB pricing](https://aws.amazon.com/dynamodb/pricing/)  


21. [Supported Virtual Machine on EC2](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/amazon-linux-2-virtual-machine.html)  


26. [Route Tables and VPN Route Priority](https://docs.aws.amazon.com/vpn/latest/s2svpn/VPNRoutingTypes.html)  


36. [Customer Support](https://aws.amazon.com/premiumsupport/plans/)

38. [Cloud Formation valid parameters](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/template-anatomy.html)  

42. [SQS visibility timeout](https://docs.aws.amazon.com/AWSSimpleQueueService/latest/SQSDeveloperGuide/sqs-visibility-timeout.html)  
