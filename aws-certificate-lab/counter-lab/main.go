package main

import (
	"fmt"
	"sort"
)

// Counting the frequency of the wrong answers and question numbers.
func main() {
	arr := []int{
		175, 6, 363, 255, 83, 183, 167, 234, 396, 136, 368, 44,
		146, 333, 59, 236, 306, 133, 165, 140, 130, 174, 132,
		291, 360, 157, 168, 82, 397, 336, 145, 283, 355, 109, 258, 252,
		243, 349, 17, 385, 303, 118, 150, 136, 177, 61, 288, 139, 363, 204,
		355, 292, 230, 84, 239, 88, 159, 246, 359, 38, 318, 69,
		43, 237, 344, 263, 6, 31, 151, 119, 246, 113, 204, 13, 153,
		347, 240, 134, 88, 451, 206, 294, 24, 178, 358, 300, 62,
		339, 150, 174, 337, 8, 191, 135, 334, 293, 168, 49, 351,
		145, 272, 455, 55,
	}
	countSort(arr)
}

func countSort(arr []int) {
	counter := make(map[int]int)

	for _, v := range arr {
		if _, ok := counter[v]; !ok {
			counter[v] = 1
		} else {
			counter[v]++
		}
	}

	var keys []int
	for key := range counter {
		keys = append(keys, key)
	}

	sort.Slice(keys, func(i, j int) bool {
		return keys[i] < keys[j]
	})

	for _, key := range keys {
		fmt.Printf("question: Q%d, %d times \n", key, counter[key])
	}
}
