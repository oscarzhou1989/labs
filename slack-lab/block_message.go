package main

import (
	"fmt"

	"github.com/nlopes/slack"
)

func GetBlocks() []slack.Block {
	var blocks []slack.Block
	blocks = append(blocks, slack.SectionBlock{
		Type: slack.MessageBlockType("section"),
		Text: &slack.TextBlockObject{
			Type: "mrkdwn",
			Text: "Danny Torrence left the following review for your property:",
		},
	})

	blocks = append(blocks, slack.NewActionBlock("block123", GetButtonBlock("action123")))

	return blocks
}

func GetButtonBlock(actionID string) slack.BlockElement {
	return slack.ButtonBlockElement{
		Type: slack.MessageElementType("button"),
		Text: &slack.TextBlockObject{
			Type: "plain_text",
			Text: "Reply to me             ",

			Emoji: false,
		},
		ActionID: actionID,
		Value:    "CNM6NDARE",
		// URL:      "https://app.slack.com/client/TMUDWMWR5/CNM6NDARE",
		URL: "https://slack.com/app_redirect?channel=CNM6NDARE",
		// URL: "slack://channel?TEAM=tmudwmwr5&id=CNM6NDARE",
	}
}

func GetNewChannelBlock(channelName, channelID string) []slack.Block {
	var blocks []slack.Block

	// Reminder
	content := fmt.Sprintf("*Reminders for* <!channel>\nA new channel 【%s】 has been created", channelName)
	textBlockObject := slack.NewTextBlockObject("mrkdwn", content, false, false)
	sectionBlock := slack.NewSectionBlock(textBlockObject, nil, nil)
	blocks = append(blocks, sectionBlock)

	// Divider
	dividerBlock := slack.NewDividerBlock()
	blocks = append(blocks, dividerBlock)

	// Content
	textBlockObject = slack.NewTextBlockObject("mrkdwn", "111", false, false)
	sectionBlock = slack.NewSectionBlock(textBlockObject, nil, nil)
	blocks = append(blocks, sectionBlock)

	// Button
	textBlockObject = slack.NewTextBlockObject("plain_text", "Reply in Channel", true, false)
	buttonBlockElem := slack.ButtonBlockElement{
		Type:     slack.MessageElementType("button"),
		Text:     textBlockObject,
		ActionID: "actionNewChannel",
		Value:    channelID,
		URL:      "https://slack.com/app_redirect?channel=" + channelID,
		Style:    slack.StylePrimary,
	}
	actionBlock := slack.NewActionBlock("blockNewChannel", buttonBlockElem)
	blocks = append(blocks, actionBlock)
	return blocks
}
