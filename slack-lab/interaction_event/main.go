package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/url"
	"strings"

	"github.com/nlopes/slack"
)

const (
	payload string = `payload=%7B%22type%22%3A%22block_actions%22%2C%22team%22%3A%7B%22id%22%3A%22TMUDWMWR5%22%2C%22domain%22%3A%22rsosworkspace%22%7D%2C%22user%22%3A%7B%22id%22%3A%22UMGHHQGR0%22%2C%22username%22%3A%22zhouhongyu1989%22%2C%22name%22%3A%22zhouhongyu1989%22%2C%22team_id%22%3A%22TMUDWMWR5%22%7D%2C%22api_app_id%22%3A%22AMS47DG9J%22%2C%22token%22%3A%22PKVnSiRjh8Liysii9UaHbMOy%22%2C%22container%22%3A%7B%22type%22%3A%22message%22%2C%22message_ts%22%3A%221569301921.006300%22%2C%22channel_id%22%3A%22CN4PWC5J6%22%2C%22is_ephemeral%22%3Afalse%7D%2C%22trigger_id%22%3A%22773861749751.742472744855.771a5e6d5dd5158d1deb3f42311adccb%22%2C%22channel%22%3A%7B%22id%22%3A%22CN4PWC5J6%22%2C%22name%22%3A%22bot-%5Cu557c%5Cu9e23%5Cu9e1f%5Cu5c0f%5Cu7a0b%5Cu5e8f%22%7D%2C%22message%22%3A%7B%22type%22%3A%22message%22%2C%22subtype%22%3A%22bot_message%22%2C%22text%22%3A%22hello%22%2C%22ts%22%3A%221569301921.006300%22%2C%22username%22%3A%22FMM-css%22%2C%22bot_id%22%3A%22BMGE5C3MG%22%2C%22blocks%22%3A%5B%7B%22type%22%3A%22section%22%2C%22block_id%22%3A%222Ba%22%2C%22text%22%3A%7B%22type%22%3A%22mrkdwn%22%2C%22text%22%3A%22Danny+Torrence+left+the+following+review+for+your+property%3A%22%2C%22verbatim%22%3Afalse%7D%7D%2C%7B%22type%22%3A%22actions%22%2C%22block_id%22%3A%22block123%22%2C%22elements%22%3A%5B%7B%22type%22%3A%22button%22%2C%22action_id%22%3A%22action123%22%2C%22text%22%3A%7B%22type%22%3A%22plain_text%22%2C%22text%22%3A%22Reply+to+me%22%2C%22emoji%22%3Atrue%7D%7D%5D%7D%5D%7D%2C%22response_url%22%3A%22https%3A%5C%2F%5C%2Fhooks.slack.com%5C%2Factions%5C%2FTMUDWMWR5%5C%2F773861749719%5C%2FVRB6incjXYeKB2L1JhRCEDRt%22%2C%22actions%22%3A%5B%7B%22action_id%22%3A%22action123%22%2C%22block_id%22%3A%22block123%22%2C%22text%22%3A%7B%22type%22%3A%22plain_text%22%2C%22text%22%3A%22Reply+to+me%22%2C%22emoji%22%3Atrue%7D%2C%22type%22%3A%22button%22%2C%22action_ts%22%3A%221569360598.408634%22%7D%5D%7D
	`
)

func main() {
	payloadBody, _ := url.QueryUnescape(strings.Split(payload, "=")[1])
	fmt.Println("===", payloadBody)

	// messageAction, err := slackevents.ParseActionEvent(payloadBody)
	// if err != nil {
	// 	log.Fatal(err)
	// }

	action := slack.InteractionCallback{}

	err := json.Unmarshal([]byte(payloadBody), &action)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("message action:", action)
}
