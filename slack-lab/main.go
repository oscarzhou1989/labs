package main

import (
	"fmt"
	"log"

	"github.com/nlopes/slack"
)

func main() {
	slackClient := slack.New("xoxp-742472744855-734225476401-730481407890-d653303e6138dce7825f2a4e80f3fd42", slack.OptionDebug(false))

	// Action: Get all groups
	groups, err := slackClient.GetGroups(false)
	if err != nil {
		log.Println(err)
		return
	}

	for _, group := range groups {
		fmt.Printf("ID:%s, Name:%s\n", group.ID, group.Name)
	}

	// Action: Get all channels
	channels, err := slackClient.GetChannels(false)
	if err != nil {
		log.Println(err)
		return
	}
	for _, channel := range channels {
		fmt.Printf("ID:%s, Name:%s\n", channel.ID, channel.Name)
	}

	// Action: Create a channel
	// channel, err := slackClient.CreateChannel("test 1")
	// if err != nil {
	// 	log.Println(err)
	// 	return
	// }
	// fmt.Printf("ID:%s, Name:%s\n", channel.ID, channel.Name)

	// Action: Send message to channel by channel name
	// msgOption := slack.MsgOptionText("post message to channel by channel name", true)
	// chn, tms, text, err := slackClient.SendMessage("bot-啼鸣鸟小程序", msgOption)
	// if err != nil {
	// 	log.Println(err)
	// 	return
	// }
	// log.Printf("channel=%s\ntimestamp=%s\ntext=%s\n", chn, tms, text)

	// Action: Send a bot message with the bot avatar
	// var msgOptions []slack.MsgOption
	// msgOptions = append(msgOptions, slack.MsgOptionAsUser(false))
	// msgOptions = append(msgOptions, slack.MsgOptionText("<!channel>", false))
	// chn, tms, text, err := slackClient.SendMessage("bot-啼鸣鸟小程序", msgOptions...)
	// if err != nil {
	// 	log.Println(err)
	// 	return
	// }
	// log.Printf("channel=%s\ntimestamp=%s\ntext=%s\n", chn, tms, text)

	// Action: List users
	users, err := slackClient.GetUsers()
	if err != nil {
		log.Println(err)
		return
	}

	for _, v := range users {
		fmt.Printf("ID:%s, Name:%s\n", v.ID, v.Name)
	}

	// Join channel

	// Action: Send block message to Channel
	// msgOptions = []slack.MsgOption{}
	// msgOptions = append(msgOptions, slack.MsgOptionAsUser(false))
	// msgOptions = append(msgOptions, slack.MsgOptionText("a new channel has been created...", false))
	// msgOptions = append(msgOptions, slack.MsgOptionBlocks(GetNewChannelBlock("team", "CNDKDSH")...))
	// chn, tms, text, err := slackClient.SendMessage("bot-啼鸣鸟小程序", msgOptions...)
	// if err != nil {
	// 	log.Println(err)
	// 	return
	// }
	// log.Printf("channel=%s\ntimestamp=%s\ntext=%s\n", chn, tms, text)

	sc := slack.New("xoxb-742472744855-768839001252-mHvJWSOw2qES2i2lOKmM0YO1", slack.OptionDebug(false))
	// // Action: Upload file
	// buf, err := ioutil.ReadFile("blue_rose.jpeg")
	// if err != nil {
	// 	log.Fatal(err)
	// }
	// reader := bytes.NewBuffer(buf)
	// params := slack.FileUploadParameters{
	// 	Filename: "blue_rose.jpeg",
	// 	Reader:   reader,
	// 	Channels: []string{"CNSGPRU22"},
	// }

	// file, err := sc.UploadFile(params)
	// if err != nil {
	// 	log.Fatal(err)
	// }
	// fmt.Println("file: ", file)

	// Action: Download file
	// ibuf := new(bytes.Buffer)
	// downloadedLink := "https://files.slack.com/files-pri/TMUDWMWR5-FNVEM9F4M/download/coffee-1.jpg"
	// if err := slackClient.GetFile(downloadedLink, ibuf); err != nil {
	// 	log.Fatal(err)
	// }
	// fmt.Println("file: ", ibuf)

	// Action: Get bot user profile
	// "UNLQP017E"
	user, err := sc.GetUserInfo("UML6ME0BT")
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(user.IsBot)

	// Action: Join a bot in a channel
	// channel: CNM6NDARE
	// bot: UML6ME0BT
	_, err = slackClient.InviteUserToChannel("CNM6NDARE", "UML6ME0BT")
	if err != nil {
		log.Fatal(err)
	}

	// Action: Rename a channel
	// CN4GFFABE
	// channel, err := slackClient.RenameChannel("CN4GFFABE", "ANNNNOSCAR")
	// if err != nil {
	// 	log.Println(err)
	// 	return
	// }
	// fmt.Printf("ID:%s, Name:%s\n", channel.ID, channel.Name)

}
