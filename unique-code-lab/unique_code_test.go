package unique_code

import (
	"testing"
)

func TestToUniqueCode(t *testing.T) {
	myStorage := &MyStorage{
		m: make(map[int]string),
	}

	for i := 0; i < 20000; i++ {
		code, err := ToUniqueCode(myStorage)
		if err != nil {
			t.Error(code)
			t.Error(err)
		}
	}

	myStorage.ToTable()

}
