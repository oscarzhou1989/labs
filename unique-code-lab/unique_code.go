package unique_code

import (
	"fmt"
	"math/rand"
	"time"
)

type Storage interface {
	Found(code *string) bool
	Save(code string) error
}

func ToUniqueCode(storage Storage) (string, error) {
	var (
		uniqueCode string
	)
	now := time.Now()
	rd := fmt.Sprintf("%d", rand.Intn((int)(now.Unix())))
	suffixNumber := rd[len(rd)-2:]
	prefixDate := fmt.Sprintf("%04d%02d%02d", now.Year(), now.Month(), now.Day())[2:]

	code := prefixDate + "&" + suffixNumber
	if !storage.Found(&code) {
		uniqueCode = "u" + prefixDate + "001" + suffixNumber
		storage.Save(uniqueCode)
		return uniqueCode, nil
	}

	numStr := code[6:9]
	var number int64
	n, err := fmt.Sscanf(numStr, "%d", &number)
	if err != nil || n == 0 {
		return "", err
	}

	numStr = fmt.Sprintf("%03d", number+1)
	if numStr == "999" {
		fmt.Println("99999")
		uniqueCode, err := ToUniqueCode(storage)
		if err != nil {
			return "", err
		}

		storage.Save(uniqueCode)
		return uniqueCode, nil
	}

	uniqueCode = "u" + prefixDate + numStr + suffixNumber
	storage.Save(uniqueCode)
	return uniqueCode, nil
}
