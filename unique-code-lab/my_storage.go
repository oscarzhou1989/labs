package unique_code

import (
	"fmt"
	"sort"
	"strings"
)

var count int = 0

type MyStorage struct {
	m map[int]string
}

func (e *MyStorage) Found(code *string) bool {
	var sortKeys []int
	keys := strings.Split(*code, "&")
	for i, v := range e.m {
		prefix := v[1:7]
		suffix := v[10:]
		if prefix == keys[0] && suffix == keys[1] {
			sortKeys = append(sortKeys, i)
		}
	}

	if len(sortKeys) != 0 {
		sort.Ints(sortKeys)
		*code = e.m[sortKeys[len(sortKeys)-1]]
		return true
	}

	return false
}

func (e *MyStorage) Save(code string) error {
	e.m[count] = code
	count++
	return nil
}

func (e *MyStorage) ToTable() {
	var m = make(map[string]int)
	for _, value := range e.m {
		if _, ok := m[value]; ok {
			m[value]++
		} else {
			m[value] = 1
		}
	}

	for i, v := range m {
		if v > 1 {
			fmt.Printf("key=%v, value=%v\n", i, v)
		}
	}
}
