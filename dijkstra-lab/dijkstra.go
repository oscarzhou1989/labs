package dijkstra

import (
	"math"
)

type neighbor map[string]float64

func DijkstraSearch(graph map[string]neighbor, costs map[string]float64, parents map[string]string) (weight float64) {
	var processed []string

	node := findLowestCostNode(costs, processed)
	for {
		if node != "" {
			nodeCost := costs[node]
			neighbors := graph[node]
			for k, v := range neighbors {
				if costs[k] > v+nodeCost {
					costs[k] = v + nodeCost
					parents[k] = node

				}
			}
			processed = append(processed, node)
			node = findLowestCostNode(costs, processed)
		} else {
			break
		}
	}
	return costs["end"]
}

func isProcessed(target string, processed []string) bool {
	for _, v := range processed {
		if target == v {
			return true
		}
	}
	return false
}

func findLowestCostNode(costs map[string]float64, processed []string) string {
	lowestCost := math.Inf(0)
	lowestCostNode := ""
	for node, cost := range costs {
		if cost < lowestCost && !isProcessed(node, processed) {
			lowestCost = cost
			lowestCostNode = node
		}
	}
	return lowestCostNode
}
