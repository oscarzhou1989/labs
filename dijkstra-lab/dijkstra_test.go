package dijkstra

import (
	"math"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestDijkstraSearch(t *testing.T) {
	graph := make(map[string]neighbor)

	ngb := make(neighbor)
	ngb["A"] = 2
	ngb["B"] = 5
	graph["start"] = ngb

	ngb = make(neighbor)
	ngb["B"] = 8
	ngb["D"] = 7
	graph["A"] = ngb

	ngb = make(neighbor)
	ngb["C"] = 4
	ngb["D"] = 2
	graph["B"] = ngb

	ngb = make(neighbor)
	ngb["D"] = 6
	ngb["end"] = 3
	graph["C"] = ngb

	ngb = make(neighbor)
	ngb["end"] = 1
	graph["D"] = ngb

	costs := make(map[string]float64)
	costs["A"] = 2
	costs["B"] = 5
	costs["C"] = math.Inf(0)
	costs["D"] = math.Inf(0)
	costs["end"] = math.Inf(0)

	parents := make(map[string]string)
	parents["A"] = "start"
	parents["B"] = "start"
	parents["end"] = ""

	var expect float64 = 8
	got := DijkstraSearch(graph, costs, parents)
	assert.Equal(t, expect, got)
}
