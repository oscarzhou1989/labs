package main

import (
	"encoding/json"
	"fmt"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sqs"
)

type MyEvent struct {
	CreatedAt string `json:"created_at"`
	Nickname  string `json:"nickname"`
	Message   string `json:"message"`
}

func HandleRequest(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	sqsURL := "https://sqs.ap-southeast-2.amazonaws.com/781078639336/serverless-message-queue"

	var obj MyEvent

	err := json.Unmarshal([]byte(request.Body), &obj)
	if err != nil {
		return events.APIGatewayProxyResponse{Body: err.Error(), StatusCode: 500}, nil
	}
	message := fmt.Sprintf(`{"created_at":"%s","nickname":"%s","message":"%s"}`, obj.CreatedAt, obj.Nickname, obj.Message)

	response, err := json.Marshal(&obj)
	if err != nil {
		return events.APIGatewayProxyResponse{Body: err.Error(), StatusCode: 500}, nil
	}

	sess, err := session.NewSession(&aws.Config{Region: aws.String("ap-southeast-2")})
	if err != nil {
		return events.APIGatewayProxyResponse{Body: err.Error(), StatusCode: 500}, nil
	}

	svc := sqs.New(sess)
	_, err = svc.SendMessage(&sqs.SendMessageInput{
		DelaySeconds: aws.Int64(10),
		MessageBody:  aws.String(message),
		QueueUrl:     &sqsURL,
	})

	if err != nil {
		return events.APIGatewayProxyResponse{Body: err.Error(), StatusCode: 500}, nil
	}

	headers := make(map[string]string)
	headers["Content-Type"] = "application/json"
	headers["Access-Control-Allow-Origin"] = "*"
	return events.APIGatewayProxyResponse{Body: string(response), Headers: headers, StatusCode: 200}, nil
}

func main() {
	lambda.Start(HandleRequest)
}
