package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/sqs"
)

type MyItem struct {
	ID        string `json:"id"`
	CreatedAt string `json:"created_at"`
	Nickname  string `json:"nickname"`
	Message   string `json:"message"`
}

func handler(ctx context.Context, sqsEvent events.SQSEvent) error {
	sqsURL := "https://sqs.ap-southeast-2.amazonaws.com/781078639336/serverless-message-queue"
	sess, err := session.NewSession(&aws.Config{Region: aws.String("ap-southeast-2")})
	if err != nil {
		fmt.Println(err)
		return err
	}

	svc := sqs.New(sess)
	// Create delete message input
	var deleteMessageInputs []sqs.DeleteMessageInput

	sdc := dynamodb.New(sess)
	tableName := "ChattingHistories"
	for _, message := range sqsEvent.Records {
		deleteMessageInputs = append(deleteMessageInputs, sqs.DeleteMessageInput{
			QueueUrl:      &sqsURL,
			ReceiptHandle: &message.ReceiptHandle,
		})

		// Save to DynamoDB

		var item MyItem
		if err := json.Unmarshal([]byte(message.Body), &item); err != nil {
			fmt.Println(err)
			return err
		}

		item.ID = message.MessageId
		av, err := dynamodbattribute.MarshalMap(item)
		if err != nil {
			fmt.Println(err)
			return err
		}

		input := &dynamodb.PutItemInput{
			Item:      av,
			TableName: aws.String(tableName),
		}

		if _, err = sdc.PutItem(input); err != nil {
			fmt.Println(err)
			return err
		}

		fmt.Printf("The message %s for event source %s = %s \n", message.MessageId, message.EventSource, message.Body)
	}

	// Delete from SQS
	for _, v := range deleteMessageInputs {
		if _, err := svc.DeleteMessage(&v); err != nil {
			log.Fatal(err)
		}
	}

	return nil
}

func main() {
	lambda.Start(handler)
}
