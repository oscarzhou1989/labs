package main

import (
	"encoding/json"
	"sort"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
)

type MyItem struct {
	CreatedAt string `json:"created_at"`
	Nickname  string `json:"nickname"`
	Message   string `json:"message"`
}

func HandleRequest(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {

	var items []MyItem
	sess, err := session.NewSession(&aws.Config{Region: aws.String("ap-southeast-2")})
	if err != nil {
		return events.APIGatewayProxyResponse{Body: err.Error(), StatusCode: 500}, nil
	}

	svc := dynamodb.New(sess)
	tableName := "ChattingHistories"

	filt := expression.Name("message").NotEqual(expression.Value(""))
	item := expression.NamesList(expression.Name("created_at"), expression.Name("nickname"), expression.Name("message"))
	expr, err := expression.NewBuilder().WithFilter(filt).WithProjection(item).Build()
	if err != nil {
		return events.APIGatewayProxyResponse{Body: err.Error(), StatusCode: 500}, nil
	}

	params := &dynamodb.ScanInput{
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
		FilterExpression:          expr.Filter(),
		ProjectionExpression:      expr.Projection(),
		TableName:                 aws.String(tableName),
	}

	result, err := svc.Scan(params)
	if err != nil {
		return events.APIGatewayProxyResponse{Body: err.Error(), StatusCode: 500}, nil
	}

	itemMap := make(map[string]MyItem)
	var keys []string
	for _, v := range result.Items {
		var item MyItem
		if err = dynamodbattribute.UnmarshalMap(v, &item); err != nil {
			return events.APIGatewayProxyResponse{Body: err.Error(), StatusCode: 500}, nil
		}

		itemMap[item.CreatedAt] = item
		keys = append(keys, item.CreatedAt)
	}

	sort.Strings(keys)

	for _, k := range keys {
		items = append(items, itemMap[k])
	}

	response, err := json.Marshal(&items)
	if err != nil {
		return events.APIGatewayProxyResponse{Body: err.Error(), StatusCode: 500}, nil
	}

	headers := make(map[string]string)
	headers["Content-Type"] = "application/json"
	headers["Access-Control-Allow-Origin"] = "*"
	return events.APIGatewayProxyResponse{Body: string(response), Headers: headers, StatusCode: 200}, nil
}

func main() {
	lambda.Start(HandleRequest)
}
