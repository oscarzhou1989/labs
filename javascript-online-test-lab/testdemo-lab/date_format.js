function formatDate(userDate) {
  // format from M/D/YYYY to YYYYMMDD
  var parts = userDate.split("/");
  var year = parseInt(parts[2],10).toString();
  var month;
  var monthInt = parseInt(parts[0],10);
  if (monthInt<10){
  	month = "0"+monthInt.toString();
  } else{
  	month = monthInt.toString();
  }
  var day;
  var dayInt = parseInt(parts[1],10);
  if (dayInt<10){
  	day = "0"+dayInt.toString();
  } else{
  	day = dayInt.toString();
  }
  return year+month+day;
}

console.log(formatDate("05/31/2014"));
console.log(formatDate("05/01/2014"));
console.log(formatDate("12/31/2014"));