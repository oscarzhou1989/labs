package main

import "fmt"

func main() {

	fmt.Printf("\a")
	fmt.Printf("\b")
	fmt.Printf("\f")
	fmt.Printf("\n")
	fmt.Printf("\r")
	fmt.Printf("\t")
	fmt.Printf("\v")
	fmt.Printf("\\")
	fmt.Printf("\"")

	//---
	var a [10]*float64
	fmt.Printf("%v", a)

	n := 5
	var arr [2 * n]struct {
		x, y int32
	} = [2*n]{struct {
		x, y int32
	}{
		x: 10,
		y: 12,
	}}
}
