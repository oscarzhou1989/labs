package main

import (
	"encoding/json"
	"fmt"

	"github.com/jinzhu/gorm/dialects/postgres"
)

type Snapshot struct {
	ID   string
	Name string
}

type Entity struct {
	Code     string
	Snapshot postgres.Jsonb
}

func main() {
	snapshot := Snapshot{
		ID:   "1111000",
		Name: "Oscar",
	}

	var entity Entity
	entity.Code = "2039"
	snp, _ := json.Marshal(snapshot)

	entity.Snapshot = postgres.Jsonb{json.RawMessage(snp)}
	fmt.Println(entity.Snapshot.RawMessage)

	var parsedSnapshot Snapshot

	json.Unmarshal(entity.Snapshot.RawMessage, &parsedSnapshot)

	fmt.Println(parsedSnapshot)

}
