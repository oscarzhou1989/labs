package main

import "fmt"

func main() {
	display(getToken, "hello", "world")
}

type tokenFunc func(...string)

func getToken(args ...string) {
	fmt.Println("arg0=", args[0])
	fmt.Println("arg1=", args[1])
}

func display(f tokenFunc, args ...string) {
	f(args...)
}
