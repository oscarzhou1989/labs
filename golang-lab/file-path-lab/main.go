package main

import (
	"fmt"
	"os"
	"path"
	"path/filepath"
	"strings"
)

func main() {
	dir, err := os.Getwd()
	if err != nil {
		panic(err)
	}

	fmt.Println(dir)
	imageURL := "http://aws.sifle/poster.jpg"
	fmt.Println(path.Base(imageURL))
	ext := filepath.Ext(imageURL)
	name := path.Base(imageURL)[:strings.Index(path.Base(imageURL), ext)]

	fmt.Println(name)

	fmt.Println(fmt.Sprintf("%d%%", 3))
}
