package main

import (
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"strings"
)

func main() {

	jsonBytes, _ := json.Marshal(userpc)
	hash := getHashFor(jsonBytes)
	fmt.Println(hash)

	jsonBytes1, _ := json.Marshal(userpc)
	hash1 := getHashFor(jsonBytes1)
	fmt.Println(hash1)

	fmt.Println("----")
}

func getHashFor(jsonBytes []byte) string {

	// It's just a sha256 hash, but we don't want people to be able to generate
	// the hashes themselves - that would encourage defeating the atomicity
	// Mix in some semi-secret data

	hasher := sha256.New()
	hasher.Write(jsonBytes)
	hasher.Write([]byte("\x70\x3f\x28\x4b\xb0\x97\x8c\xf3\x53\xf7\xa9\x9e"))
	rawHash := hasher.Sum(nil)
	hexHash := strings.ToLower(hex.EncodeToString(rawHash[:]))
	return hexHash

}

const userpc string = `{customer2 customer2233 Pacific/Auckland en_US [] map[] false map[65b908a:{Storage provided by (...) 1582584755 1582584755 [] [] {1003 {http://127.0.0.1:8060/ 9c9e30d8-91cc-b7-e997-48d6-96db-d1b4549198bf} { false     false} {   0   false } {   false  0 0 0 false} {    } {   0} 2002 LLJfe8LQU/pog7AhPgsOlQLB5rb9KpF7tgw5Xvc1/oGXhqG+IZbLTmfS9e/EWKZ7GwCTVeaez2CBR7svwWFUrvtmAAUxJ2cA 158478 1583982478} {0 0 []} 57cbf5fc-f0cf-469a-b580-a7ea9ec82f82 1583982476 1583982476} {801 []}} 92927C2E-9ws s3 Storage Vault (Thu Feb 27 09:32:04 2020) 1582749124 1582749859 [] [] {1000 {  } {s3.amazonaws.com fHshqluBwDsMack6zuqZjY0CdJFdokEHI6kI70 cometbackup-test  false} {   0   false } {   false  0 0 0 false} {    0} {[]}} 2002 /K14ZJ12nauGDKCYvrdCeRuyLMzmf8Fy0ZEfqBa0P+YKCAARZ91XR+6+IBMFn++eqTqget/06+yIXBSApxbE3l1u{6500 1583707985 1583707985} {0 0 []} 9f8b780b-8350-4930-b5a8-bb6aa6e5794a 1582749868 1582749868} {801 []A0-E337C8AE38E9:{engine1/file a text doc  (Thu Feb 27 09:40:06 2020) 392f274db87c57093cd0ddb4fb7edd28bd35 [] map[INCLUDE-0:C:/Users/User/Downloads/customer2 backup test for s3 storage.txt USE_WIN_VSS:1] map[] {a6e5794a customer2 4001 5000 1582749866 1582749876 32ABEBA8-F577-47F5-A3A0-E337C8AE38E9 92927C2E-9356-46487c57093cd0ddb4fb7edd28bd35b0c0 fb47064bde944815a76b67bcbd1fd9ab28265eed30085ba64346f628e273f438 19.12.8 3988C4A-0B33-493E-96C6-04AF58FE57F9:{engine1/file some files 392f274db87c57093cd0ddb4fb7edd28bd35b0c0 158INCLUDE-0:C:/Users/Oscar/Downloads/cometbackup-docs USE_WIN_VSS:1] map[] {{04f65ecb-204a-4843-9109-eaff7c83707977 1583707987 43988C4A-0B33-493E-96C6-04AF58FE57F9 92927C2E-9356-4645-91C5-61D99FBC44AF 392f274db87 fadcd2bce3ddcdacf982245090a0b0cd85d1dee90b9d61b463eed26a5643782e 19.12.8 1 5 1834 0 4435 1617  <nil>}}} AF24D96:{engine1/file large file 392f274db87c57093cd0ddb4fb7edd28bd35b0c0 1583982363 1583982467 [] [] mapnloads/SlackSetup.exe] map[] {{57cbf5fc-f0cf-469a-b580-a7ea9ec82f82 customer2 4001 5000 1583982476 158398557F4AF24D96 658ed211-92f2-4086-b856-c055566b908a 392f274db87c57093cd0ddb4fb7edd28bd35b0c0 c23506e42abd68cbb9a2c0b3f81824bf621 20.3.0 0 1 71847952 0 71770422 1200  <nil>}}}] map[0DC33AFE-214F-4DC1-82C4-966D43589 1583706313 [] [] 43988C4A-0B33-493E-96C6-04AF58FE57F9 92927C2E-9356-4645-91C5-61D99FBC44AF {false 0 0 false}} BA6F4232-B243-4981-B10E-928860B9CA3A:{slack.exe 1583982445 1583982459 [] [] 905E253C-FA71-4FD0-BD44086-b856-c055566b908a {false 0 0 false false false} [] {false false}} DF300704-1076-487C-9A17-2AB24CB8E3ule (Thu Feb 27 09:43:09 2020) 1582749789 1582749808 [] [] 32ABEBA8-F577-47F5-A3A0-E337C8AE38E9 92927C2E-false 0 0 false false false} [] {false false}}] map[392f274db87c57093cd0ddb4fb7edd28bd35b0c0:{test2 Windo] false false 0 0  {false false false false {false []} false false false {false []} [] 0 3 0 false true ffalse false {false {[]}} {801 []} false false} 102 JDJhJDEwJFlOUlE1dWlsQk1ZTTB5RUVUMmZNSS40S2VKUXRyY1FKUHj7lKuVFUm+cwFhbWLldZXjusRC588I true false false 2002 1sK3OldF1fl4cEgPcXIn6QhRO56iirPscTb2FSghxLNDi2leIqs7F7WakI64Eu8NZy8 1582584755 c9957360-dbfd-4b80-ae84-4e8b8703bf53}`

const newpc string = `{customer2 customer2233 Pacific/Auckland en_US [] map[] false map[65b908a:{Storage provided by (...) 1582584755 1582584755 [] [] {1003 {http://127.0.0.1:8060/ 9c9e30d8-91cc-b7-e997-48d6-96db-d1b4549198bf} { false     false} {   0   false } {   false  0 0 0 false} {    } {   0} 2002 LLJfe8LQU/pog7AhPgsOlQLB5rb9KpF7tgw5Xvc1/oGXhqG+IZbLTmfS9e/EWKZ7GwCTVeaez2CBR7svwWFUrvtmAAUxJ2cA 158478 1583982478} {0 0 []} 57cbf5fc-f0cf-469a-b580-a7ea9ec82f82 1583982476 1583982476} {801 []}} 92927C2E-9ws s3 Storage Vault (Thu Feb 27 09:32:04 2020) 1582749124 1582749859 [] [] {1000 {  } {s3.amazonaws.com fHshqluBwDsMack6zuqZjY0CdJFdokEHI6kI70 cometbackup-test  false} {   0   false } {   false  0 0 0 false} {    0} {[]}} 2002 /K14ZJ12nauGDKCYvrdCeRuyLMzmf8Fy0ZEfqBa0P+YKCAARZ91XR+6+IBMFn++eqTqget/06+yIXBSApxbE3l1u{6500 1583707985 1583707985} {0 0 []} 9f8b780b-8350-4930-b5a8-bb6aa6e5794a 1582749868 1582749868} {801 []A0-E337C8AE38E9:{engine1/file a text doc  (Thu Feb 27 09:40:06 2020) 392f274db87c57093cd0ddb4fb7edd28bd35 [] map[INCLUDE-0:C:/Users/User/Downloads/customer2 backup test for s3 storage.txt USE_WIN_VSS:1] map[] {a6e5794a customer2 4001 5000 1582749866 1582749876 32ABEBA8-F577-47F5-A3A0-E337C8AE38E9 92927C2E-9356-46487c57093cd0ddb4fb7edd28bd35b0c0 fb47064bde944815a76b67bcbd1fd9ab28265eed30085ba64346f628e273f438 19.12.8 3988C4A-0B33-493E-96C6-04AF58FE57F9:{engine1/file some files 392f274db87c57093cd0ddb4fb7edd28bd35b0c0 158INCLUDE-0:C:/Users/Oscar/Downloads/cometbackup-docs USE_WIN_VSS:1] map[] {{04f65ecb-204a-4843-9109-eaff7c83707977 1583707987 43988C4A-0B33-493E-96C6-04AF58FE57F9 92927C2E-9356-4645-91C5-61D99FBC44AF 392f274db87 fadcd2bce3ddcdacf982245090a0b0cd85d1dee90b9d61b463eed26a5643782e 19.12.8 1 5 1834 0 4435 1617  <nil>}}} AF24D96:{engine1/file large file 392f274db87c57093cd0ddb4fb7edd28bd35b0c0 1583982363 1583982467 [] [] mapnloads/SlackSetup.exe] map[] {{57cbf5fc-f0cf-469a-b580-a7ea9ec82f82 customer2 4001 5000 1583982476 158398557F4AF24D96 658ed211-92f2-4086-b856-c055566b908a 392f274db87c57093cd0ddb4fb7edd28bd35b0c0 c23506e42abd68cbb9a2c0b3f81824bf621 20.3.0 0 1 71847952 0 71770422 1200  <nil>}}}] map[0DC33AFE-214F-4DC1-82C4-966D43589 1583706313 [] [] 43988C4A-0B33-493E-96C6-04AF58FE57F9 92927C2E-9356-4645-91C5-61D99FBC44AF {false 0 0 false}} BA6F4232-B243-4981-B10E-928860B9CA3A:{slack.exe 1583982445 1583982459 [] [] 905E253C-FA71-4FD0-BD44086-b856-c055566b908a {false 0 0 false false false} [] {false false}} DF300704-1076-487C-9A17-2AB24CB8E3ule (Thu Feb 27 09:43:09 2020) 1582749789 1582749808 [] [] 32ABEBA8-F577-47F5-A3A0-E337C8AE38E9 92927C2E-false 0 0 false false false} [] {false false}}] map[392f274db87c57093cd0ddb4fb7edd28bd35b0c0:{test2 Windo] false false 0 0  {false false false false {false []} false false false {false []} [] 0 3 0 false true ffalse false {false {[]}} {801 []} false false} 102 JDJhJDEwJFlOUlE1dWlsQk1ZTTB5RUVUMmZNSS40S2VKUXRyY1FKUHj7lKuVFUm+cwFhbWLldZXjusRC588I true false false 2002 1sK3OldF1fl4cEgPcXIn6QhRO56iirPscTb2FSghxLNDi2leIqs7F7WakI64Eu8NZy8 1582584755 c9957360-dbfd-4b80-ae84-4e8b8703bf53}`
