package main

import (
	"fmt"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

type Animal struct {
	Genre       string
	Environment string
}

var animals []Animal

func init() {
	animals = append(animals, Animal{
		"dog", "land",
	})
	animals = append(animals, Animal{
		"goldfish", "water",
	})
	animals = append(animals, Animal{
		"hawk", "sky",
	})
}

func refreshToken(db *gorm.DB, t time.Time) {
	ticker := time.NewTicker(time.Second * 5)
	defer ticker.Stop()
	var frame int = 1
	for {
		select {
		case <-ticker.C:
			now := time.Now()
			fmt.Printf("%d  %v tick...\n", frame, now)
			// if err := db.Exec("select * from client_profiles").Error; err != nil {
			// 	log.Println("error=", err)
			// }
			frame++
			ticker = time.NewTicker(time.Minute * 10)
		default:

		}
	}
}

func main() {
	// conn := "host=192.168.87.130 user=timwlxapi dbname=timwlxapi sslmode=disable password=timwlxapi123"
	// db, err := gorm.Open("postgres", conn)
	// if err != nil {
	// 	log.Fatal(err)
	// }

	// go refreshToken(db, time.Now())

	go refreshToken(nil, time.Now())
	r := gin.New()
	r.Handle(http.MethodGet, "/animals", Get)
	r.Run(":7000")

}

func Get(ctx *gin.Context) {
	if len(animals) == 0 {
		ctx.JSON(http.StatusNoContent, nil)
	}

	ctx.JSON(http.StatusOK, animals)
	return
}
