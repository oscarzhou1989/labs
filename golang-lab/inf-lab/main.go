package main

import (
	"fmt"
	"math"
)

func main() {
	f := math.Inf(1)
	fmt.Println("f====", f > 387892397)

	var k int = 475
	fmt.Println("k====", float64(k*1.00))

	f = float64(k)
	fmt.Println("f====", f)
	fmt.Println("f====", int64(f))
	// f = int64(math.Inf(-1))
	// fmt.Println("f====", f)
	// fmt.Println("f====", f > 1022339)
	// f = 1022339
	// fmt.Println("f====", f)

}
