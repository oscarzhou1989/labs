package main

import (
	"fmt"
	"strings"
)

// func main() {
// 	s := filepath.Join("a", "b", "c")
// 	fmt.Println(s)
// }

func Join(params ...string) string {
	return strings.Join(params, PathSeparator)
}

func main() {
	params := []string{"a", "b", "c"}
	fmt.Println(Join(params...))
}
