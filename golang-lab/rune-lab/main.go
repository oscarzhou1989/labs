package main

import "fmt"

func main() {
	s := "abc"
	ss := []rune(s)
	for _, v := range ss {
		fmt.Println(v)
	}

	fmt.Println(string(ss[0:2]))

	kk := make([]rune, len(ss))
	copy(kk, ss)
	fmt.Println("ss before:", ss)
	fmt.Println("kk before:", kk)
	fmt.Println("change kk")
	kk[0], kk[2] = kk[2], kk[0]
	fmt.Println("ss after:", ss)
	fmt.Println("kk after:", kk)

}
