package main

import (
	"encoding/json"
	"fmt"
	"log"

	"github.com/nlopes/slack/slackevents"
	uuid "go.uuid"
)

const (
	text string = `{"workspace":{"ID":"0e972ade-4cc7-452d-889d-b389a330c923","CreatedAt":"2019-09-09T03:36:54.515703Z","UpdatedAt":"2019-09-09T03:38:05.039433Z","OwnerID":"0a6aa593-9f8d-4708-8934-37f4935a2754","OwnerType":"agencies","Domain":"rsosworkspace.slack.com","AppName":"FMM-css","AppID":"AMS47DG9J","ClientID":"742472744855.740143458324","ClientSecret":"09281f8d8ca0617411d17ad5c22a7a7a","SigningSecret":"05511abd24e7b685d01efe73bdc01d82","VerificationToken":"PKVnSiRjh8Liysii9UaHbMOy","OAuthAccessToken":"xoxp-742472744855-734225476401-730481407890-d653303e6138dce7825f2a4e80f3fd42","AdminEmail":"oscar.z@dataant.io","PrivateChannel":false,"GlobalDefault":true},"ev":{"type":"message","user":"UML6ME0BT","text":"好好用一颗爱我222","thread_ts":"","ts":"1569070462.000500","channel":"CNB5T7EBT","channel_type":"channel","event_ts":1569070462.000500,"upload":false,"files":null},"open_id":"ooxT70Di7mCZ0bltitcAYJ3hqR-s"}`
)

const (
	ImageMaxSize int = 1024 * 1024 * 10
)

func main() {
	var sm SlackMessage
	if err := json.Unmarshal([]byte(text), &sm); err != nil {
		log.Fatal(err)
	}

	fmt.Println(sm)
	fmt.Println(*sm.Workspace)
	fmt.Println(*sm.EventMessage)

	// // Read the picture to bytes
	// bytes, err := ioutil.ReadFile("blue_rose.jpeg")
	// if err != nil {
	// 	panic(err)
	// }

	// // Unmarshal the bytes to a []byte variable
	// imageBytes := make([]byte, ImageMaxSize)
	// if err = json.Unmarshal(bytes, imageBytes); err != nil {
	// 	panic(err)
	// }
}

type SlackMessage struct {
	Workspace    *Workspace                `json:"workspace"`
	EventMessage *slackevents.MessageEvent `json:"ev"`
	OpenID       string                    `json:"open_id"`
}

type Workspace struct {
	ID                uuid.UUID
	OwnerID           uuid.UUID `gorm:"type:uuid"`
	OwnerType         string    `json:",read_only"`
	Domain            string    `validate:"required"`
	AppName           string    `validate:"required"`
	AppID             string    `validate:"required"`
	ClientID          string
	ClientSecret      string
	SigningSecret     string
	VerificationToken string `validate:"required"`
	OAuthAccessToken  string `validate:"required"`
	AdminEmail        string `validate:"email"`
	PrivateChannel    bool
	GlobalDefault     bool `json:",read_only" gorm:"default:false"`
}
