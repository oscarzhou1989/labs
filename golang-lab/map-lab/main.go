package main

import "fmt"

func main() {

	graph := make(map[string]float64)

	graph["A"] = 2
	graph["B"] = 4

	for k, v := range graph {
		fmt.Printf("k=%s, v=%f\n", k, v)
	}
}
