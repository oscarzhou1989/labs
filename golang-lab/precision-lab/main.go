package main

import "fmt"

func main() {
	var rebate int64 = 500
	fmt.Printf("%02f\n", float64(rebate)*0.01)
	fmt.Printf("%.02f\n", float64(rebate)*0.01)
}
