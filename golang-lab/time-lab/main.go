package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main() {
	timeZoneTest()

}

func timeZoneTest() {
	aucklandTimeZone := "Pacific/Auckland"
	shanghaiTimeZone := "Asia/Shanghai"

	now := time.Now()
	fmt.Println("now = ", now)

	fmt.Println("utc = ", now.UTC())

	loc, _ := time.LoadLocation(shanghaiTimeZone)
	shanghaiTime := now.In(loc)
	fmt.Println("shanghai = ", shanghaiTime)

	loc, _ = time.LoadLocation(aucklandTimeZone)
	aucklandTime := now.In(loc)
	fmt.Println("auckland = ", aucklandTime)
}

func randTime() {
	for i := 0; i < 10; i++ {
		now := time.Now()
		rd := fmt.Sprintf("%d", rand.Intn((int)(now.Unix())))
		fmt.Println(rd)
		suffixNumber := rd[len(rd)-2:]
		fmt.Println("=", suffixNumber)

	}

}
