package main

import (
	"encoding/json"
	"fmt"

	"github.com/aws/aws-lambda-go/lambda"
)

type MyEvent struct {
	Name string `json:"name"`
}

type MyBody struct {
	Message string `json:"message"`
}

type MyResponse struct {
	StatusCode      int    `json:"statusCode"`
	Headers         []byte `json:"headers"`
	Body            []byte `json:"body"`
	IsBase64Encoded bool   `json:"isBase64Encoded"`
}

func HandleRequest(event MyEvent) (MyResponse, error) {
	body := MyBody{Message: fmt.Sprintf("Hello %s!", event.Name)}
	result, _ := json.Marshal(body)

	header := make(map[string]string)
	header["Content-Type"] = "application/json"
	h, _ := json.Marshal(header)

	response := MyResponse{
		StatusCode:      200,
		Headers:         h,
		Body:            result,
		IsBase64Encoded: false,
	}

	return response, nil
}

func main() {
	lambda.Start(HandleRequest)
}
