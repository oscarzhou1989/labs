package main

import (
	"context"
	"fmt"
	"os"

	"github.com/aws/aws-lambda-go/lambda"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	uuid "go.uuid"
)

type User struct {
	ID         uuid.UUID `json:"id"`
	UniqueCode string    `json:"unique_code"`
}

func GetConnection() (db *gorm.DB, err error) {
	username := os.Getenv("db_user")
	password := os.Getenv("db_pass")
	dbName := os.Getenv("db_name")
	dbHost := os.Getenv("db_host")
	dbURI := fmt.Sprintf("host=%s user=%s dbname=%s sslmode=disable password=%s",
		dbHost, username, dbName, password)
	return gorm.Open("postgres", dbURI)
}

func HandleRequest(ctx context.Context, myUser User) (User, error) {

	db, err := GetConnection()
	if err != nil {
		return User{}, err
	}
	defer db.Close()

	var user User
	if err := db.Raw("select id, unique_code from users where unique_code = ?", myUser.UniqueCode).
		Scan(&user).Error; err != nil {
		return User{}, err
	}

	if err := db.Exec("delete from coupon_referrers where referrer_unique_code = ? or user_id = ?",
		user.UniqueCode, user.ID).Error; err != nil {
		return User{}, err
	}

	if err := db.Exec("delete from coupon_records where user_id = ?", user.ID).
		Error; err != nil {
		return User{}, err
	}

	if err := db.Exec("delete from affiliate_partner_requests where user_id = ?", user.ID).
		Error; err != nil {
		return User{}, err
	}

	if err := db.Exec("delete from affiliate_partners where user_id = ?", user.ID).
		Error; err != nil {
		return User{}, err
	}

	if err := db.Exec("update users set role_id = (select id from roles where name = ?) where id = ?",
		"MB", user.ID).Error; err != nil {
		return User{}, err
	}

	if err := db.Exec("update accounts set type = 1, balance = 100000000, rebate_balance = 0, withdrawable_rebate = 0 where user_id = ?", user.ID).
		Error; err != nil {
		return User{}, err
	}

	return user, nil
}

func main() {
	lambda.Start(HandleRequest)
}
