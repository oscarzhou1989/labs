package main

import (
	"fmt"

	"github.com/aws/aws-lambda-go/lambda"
)

type MyEvent struct {
	Name string `json:"name"`
}

type MyResponse struct {
	Message string `json:"message"`
}

func HandleRequest(event MyEvent) (MyResponse, error) {
	body := MyResponse{Message: fmt.Sprintf("Hello %s!", event.Name)}
	return body, nil
}

func main() {
	lambda.Start(HandleRequest)
}
