package main

import (
	"context"
	"fmt"

	"github.com/aws/aws-lambda-go/lambda"
)

type MyEvent struct {
	Message string `json:"message"`
}

func HandleRequest(ctx context.Context, message MyEvent) (string, error) {
	return fmt.Sprintf("Hello world! My message is :%s!", message.Message), nil
}

func main() {
	lambda.Start(HandleRequest)
}
