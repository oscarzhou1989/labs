package main

import (
	"fmt"

	"github.com/aws/aws-lambda-go/lambda"
)

type MyResponse struct {
	StatusCode int               `json:"statusCode"`
	Headers    map[string]string `json:"headers"`
	Body       string            `json:"body"`
}

func HandleRequest() (MyResponse, error) {
	header := make(map[string]string)
	header["Content-Type"] = "application/json"
	header["Access-Control-Allow-Origin"] = "*"
	response := MyResponse{
		StatusCode: 200,
		Headers:    header,
		Body:       fmt.Sprintf("Hello oscar zhou!"),
	}

	// result, _ := json.Marshal(response)

	return response, nil
}

func main() {
	lambda.Start(HandleRequest)
}
