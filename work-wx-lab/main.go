package main

import (
	"net/http"
	"timingniao_wlx_css/libraries/models"

	"github.com/gin-gonic/gin"
	workwx "github.com/xen0n/go-workwx"
)

type Message struct {
	Content string
}

const (
	corpID     string = "ww80e5b338e7a1c848"
	corpSecret string = "fKlcTPWQUQADoutMzLzfWLytMYxK0GtOdNT-AI9MLVs"
	agentID    int64  = 1000003
)

var app *workwx.WorkwxApp

func main() {
	engine := gin.New()
	client := workwx.New(corpID)
	app = client.WithApp(corpSecret, agentID)
	app.SpawnAccessTokenRefresher()

	engine.Handle(http.MethodGet, "/messages", Get)
	engine.Handle(http.MethodPost, "/messages", Post)

	engine.Run(":7000")
}

func Get(ctx *gin.Context) {
	// user, err := app.GetUser("anoscar")
	deptInfo, err := app.ListDepts(2)
	if err != nil {

	}

	// if len() == 0 {
	// 	ctx.JSON(http.StatusNoContent, nil)
	// }

	ctx.JSON(http.StatusOK, deptInfo)
	return
}

func Post(ctx *gin.Context) {
	var message models.Message
	if err := ctx.Bind(&message); err != nil {
		ctx.Err()
		return
	}

	to := workwx.Recipient{
		TagIDs: []string{"1"},
	}

	// to := workwx.Recipient{
	// 	UserIDs: []string{"anoscar", "box", "PengYilenny", "GenBenYingJun"},
	// }
	response := app.SendTextMessage(&to, message.Content, false)

	ctx.JSON(http.StatusOK, response)
	return
}
