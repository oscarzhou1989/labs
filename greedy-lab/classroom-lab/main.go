package main

import "fmt"

type Class struct {
	Name    string
	StartAt int
	EndAt   int
}

func main() {
	classes := []Class{
		Class{
			Name:    "美术",
			StartAt: 900,
			EndAt:   1000,
		},
		Class{
			Name:    "英语",
			StartAt: 930,
			EndAt:   1030,
		},
		Class{
			Name:    "数学",
			StartAt: 1000,
			EndAt:   1100,
		},
		Class{
			Name:    "计算机",
			StartAt: 1030,
			EndAt:   1130,
		},
		Class{
			Name:    "音乐",
			StartAt: 1100,
			EndAt:   1200,
		},
	}

	var (
		processed []Class
		curClass  Class = Class{}
		prevClass Class
	)
	for _, v := range classes {
		if (curClass == Class{}) {
			curClass = v
			prevClass = v
			processed = append(processed, curClass)
		} else {
			if v.StartAt >= prevClass.EndAt {
				processed = append(processed, v)
				prevClass = v
			}
		}

	}

	fmt.Println(processed)

}
