package selectionsort

import (
	"reflect"
	"testing"
)

func TestSelectionSort(t *testing.T) {
	var (
		arr    = []int{29, 10, 14, 37, 13}
		expect = []int{10, 13, 14, 29, 37}
	)
	actual := SelectionSort(arr)
	if !reflect.DeepEqual(expect, actual) {
		t.Errorf("result wrong:\nexpect=%v\nactual=%v",
			expect, actual)
	}
}
