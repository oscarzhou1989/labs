package selectionsort

func SelectionSort(arr []int) []int {
	start, last := 0, len(arr)
	for start < last {
		min := 0
		for j := start; j < last; j++ {
			if arr[min] > arr[j] || j == start {
				min = j
			}
		}

		arr[start], arr[min] = arr[min], arr[start]
		start++
	}

	return arr
}
