package bubblesort

import (
	"reflect"
	"testing"
)

func TestBubbleSort(t *testing.T) {
	var (
		arr    = []int{29, 10, 14, 37, 14}
		expect = []int{10, 14, 14, 29, 37}
	)
	actual := BubbleSort(arr)
	if !reflect.DeepEqual(expect, actual) {
		t.Errorf("result wrong:\nexpect=%v\nactual=%v",
			expect, actual)
	}
}
