package insertionsort

import (
	"reflect"
	"testing"
)

func TestInsertionSort(t *testing.T) {
	var (
		arr    = []int{29, 10, 14, 37, 13}
		expect = []int{10, 13, 14, 29, 37}
	)
	actual := InsertionSort(arr)
	if !reflect.DeepEqual(expect, actual) {
		t.Errorf("result wrong:\nexpect=%v\nactual=%v",
			expect, actual)
	}
}
