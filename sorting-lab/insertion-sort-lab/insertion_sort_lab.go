package insertionsort

// InsertionSort good explaination is https://www.youtube.com/watch?v=OGzPmgsI-pQ&feature=youtu.be
func InsertionSort(arr []int) []int {
	length := len(arr)
	for i := 1; i < length; i++ {
		cur := arr[i]
		marker := i

		for marker > 0 && arr[marker-1] > cur {
			arr[marker] = arr[marker-1]
			marker--
		}

		arr[marker] = cur
	}

	return arr
}
