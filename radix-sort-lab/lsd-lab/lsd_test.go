package lsd

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestRadixSort(t *testing.T) {

	expect := []int{2, 11, 11, 12, 24, 27, 33, 39, 51, 60, 181}
	input := []int{181, 51, 11, 33, 11, 39, 60, 2, 27, 24, 12}

	got := RadixSort(input)
	assert.Equal(t, expect, got)
}
