package lsd

import "fmt"

func getMax(arr []int) (max int) {
	max = 0
	for _, v := range arr {
		if max < v {
			max = v
		}
	}
	return
}

func RadixSort(arr []int) []int {
	max := getMax(arr)
	for exp := 1; max/exp > 0; exp *= 10 {
		countSortArray(&arr, exp)
		fmt.Println("===", &arr)
	}
	return arr
}

func countSortArray(arr *[]int, exp int) {
	length := len(*arr)
	var sortArr = make([][]int, length)

	for _, v := range *arr {
		x := (v / exp) % 10
		sortArr[x] = append(sortArr[x], v)
	}

	var sortedArr []int
	for _, v := range sortArr {
		sortedArr = append(sortedArr, v...)
	}

	(*arr) = sortedArr
}
