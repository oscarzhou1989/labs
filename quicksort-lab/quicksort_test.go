package quicksorting

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestQuicksorting(t *testing.T) {
	expect := []int{2, 3, 4, 5, 7, 8, 8, 10, 66}

	input := []int{8, 66, 4, 8, 7, 10, 5, 2, 3}
	got := QuickSorting1(input)

	assert.Equal(t, expect, got)
}
