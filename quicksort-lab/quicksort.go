package quicksorting

func QuickSorting(arr []int) []int {
	if len(arr) < 2 {
		return arr
	}
	min := len(arr) / 2
	var leftArry, rightArr []int
	for i, v := range arr {
		if i == min {
			continue
		}
		if v < arr[min] {
			leftArry = append(leftArry, v)
		} else {
			rightArr = append(rightArr, v)
		}
	}

	var retArray []int
	retArray = append(retArray, QuickSorting(leftArry)...)
	retArray = append(retArray, arr[min])
	retArray = append(retArray, QuickSorting(rightArr)...)
	return retArray
}

func QuickSorting1(arr []int) []int {
	if len(arr) < 2 {
		return arr
	}
	pivot := arr[len(arr)/2]

	var leftArr, rightArr []int
	for i, v := range arr {

		if i == len(arr)/2 {
			continue
		}
		if v < pivot {
			leftArr = append(leftArr, v)
		} else {
			rightArr = append(rightArr, v)
		}
	}

	var newArr []int
	newArr = append(newArr, QuickSorting1(leftArr)...)
	newArr = append(newArr, pivot)
	newArr = append(newArr, QuickSorting1(rightArr)...)
	return newArr
}
