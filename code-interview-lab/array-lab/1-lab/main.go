package main

import "fmt"

// How do you find the missing number in a given integer array of 1 to 100
func main() {
	input := []int{1, 2, 3, 5}

	fmt.Println("simple version: ", FindMissingOneSimple(input, 5))

	input = []int{1, 4, 2, 5, 7, 9}
	fmt.Println("more than one missing numbers: ", FindMissingMore(input, 9))

	input = []int{1, 7, 4, 2, 2, 5, 7, 9}
	fmt.Println("more than one duplicates: ", FindDuplicates(input, 9))

	input = []int{1, 7, 2, 2, 5, 7, 9}
	fmt.Println("is duplication: ", isDuplication(input))
}

// simple version: only one missing number
func FindMissingOneSimple(arr []int, n int) int {
	sum := n * (n + 1) / 2

	var newSum int
	for _, v := range arr {
		newSum += v
	}
	return sum - newSum
}

// follow-up version: more than one missing number
func FindMissingMore(arr []int, n int) []int {
	var buckets = make(map[int]int, n)

	for i := 0; i < n; i++ {
		buckets[i] = 0
	}

	for _, v := range arr {
		buckets[v-1]++
	}

	var missingNumbers []int
	for index, count := range buckets {
		if count == 0 {
			missingNumbers = append(missingNumbers, index+1)
		}
	}
	return missingNumbers
}

// follow-up version: duplicated number
func FindDuplicates(arr []int, n int) []int {
	var buckets = make(map[int]int, n)

	for i := 0; i < n; i++ {
		buckets[i] = 0
	}

	for _, v := range arr {
		buckets[v-1]++
	}

	var duplicatedNumbers []int
	for index, count := range buckets {
		if count > 1 {
			duplicatedNumbers = append(duplicatedNumbers, index+1)
		}
	}

	return duplicatedNumbers
}

// follow-up version: if there are duplicates
func isDuplication(arr []int) bool {
	var set = make(map[int]bool)

	for _, v := range arr {
		if _, exist := set[v]; !exist {
			set[v] = true
		} else {
			return true
		}
	}
	return false
}
