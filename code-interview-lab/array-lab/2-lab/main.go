package main

import "fmt"

// How do you find the duplicate number of a given integer array

func main() {
	input := []int{1, 1, 2, 2, 3, 4, 5}
	fmt.Println("output: ", removeDuplicates(input))
}

func removeDuplicates(arr []int) []int {

	arr = quickSort(arr)
	var newArr []int
	previous := arr[0]

	newArr = append(newArr, previous)
	for i := 1; i < len(arr); i++ {
		if arr[i] != previous {
			newArr = append(newArr, arr[i])
		}
		previous = arr[i]
	}

	return newArr
}

func quickSort(arr []int) []int {
	if len(arr) < 2 {
		return arr
	}
	pivot := len(arr) / 2

	var left, right []int
	for i, v := range arr {
		if i == pivot {
			continue
		}

		if v < arr[pivot] {
			left = append(left, v)
		} else {
			right = append(right, v)
		}
	}

	var newRet []int
	newRet = append(newRet, quickSort(left)...)
	newRet = append(newRet, arr[pivot])
	newRet = append(newRet, quickSort(right)...)
	return newRet
}
