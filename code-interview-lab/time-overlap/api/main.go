package main

import (
	"encoding/json"
	"io"
	overlap "labs/code-interview-lab/time-overlap"
	"log"
	"net/http"
)

func main() {

	timeRangeCheckHandler := func(w http.ResponseWriter, req *http.Request) {

		if req.Method == "POST" {
			var tr []overlap.TimeRange

			// unmarshal request body to tr
			decoder := json.NewDecoder(req.Body)
			decoder.Decode(&tr)

			if overlap.IsConflictOfMultiple(tr) {
				w.WriteHeader(500)
				io.WriteString(w, "overlap")
				return
			}
		}
		w.WriteHeader(200)
		io.WriteString(w, "no overlap")

	}

	http.HandleFunc("/TimeRangeChecks", timeRangeCheckHandler)

	log.Fatal(http.ListenAndServe(":8080", nil))
}
