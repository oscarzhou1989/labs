package overlap

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestTimeRange(t *testing.T) {

	now := time.Now()
	// case 1
	// r1    |--------|
	// r2  |----|
	r1 := TimeRange{
		StartAt: time.Date(now.Year(), now.Month(), now.Day(), 9, 0, 0, 0, time.UTC),
		EndAt:   time.Date(now.Year(), now.Month(), now.Day(), 10, 0, 0, 0, time.UTC),
	}

	r2 := TimeRange{
		StartAt: time.Date(now.Year(), now.Month(), now.Day(), 8, 0, 0, 0, time.UTC),
		EndAt:   time.Date(now.Year(), now.Month(), now.Day(), 9, 30, 0, 0, time.UTC),
	}

	expect := true
	got := IsConflict(r1, r2)
	assert.Equal(t, expect, got)

	// case 2
	// r1    |--------|
	// r2      |---|
	r1 = TimeRange{
		StartAt: time.Date(now.Year(), now.Month(), now.Day(), 9, 0, 0, 0, time.UTC),
		EndAt:   time.Date(now.Year(), now.Month(), now.Day(), 10, 0, 0, 0, time.UTC),
	}

	r2 = TimeRange{
		StartAt: time.Date(now.Year(), now.Month(), now.Day(), 9, 10, 0, 0, time.UTC),
		EndAt:   time.Date(now.Year(), now.Month(), now.Day(), 9, 30, 0, 0, time.UTC),
	}
	expect = true
	got = IsConflict(r1, r2)
	assert.Equal(t, expect, got)

	// case 3
	// r1    |--------|
	// r2   |----------|
	r1 = TimeRange{
		StartAt: time.Date(now.Year(), now.Month(), now.Day(), 9, 0, 0, 0, time.UTC),
		EndAt:   time.Date(now.Year(), now.Month(), now.Day(), 10, 0, 0, 0, time.UTC),
	}

	r2 = TimeRange{
		StartAt: time.Date(now.Year(), now.Month(), now.Day(), 8, 10, 0, 0, time.UTC),
		EndAt:   time.Date(now.Year(), now.Month(), now.Day(), 11, 30, 0, 0, time.UTC),
	}
	expect = true
	got = IsConflict(r1, r2)
	assert.Equal(t, expect, got)

	// case 4
	// r1    |--------|
	// r2       |--------|
	r1 = TimeRange{
		StartAt: time.Date(now.Year(), now.Month(), now.Day(), 9, 0, 0, 0, time.UTC),
		EndAt:   time.Date(now.Year(), now.Month(), now.Day(), 10, 0, 0, 0, time.UTC),
	}

	r2 = TimeRange{
		StartAt: time.Date(now.Year(), now.Month(), now.Day(), 9, 10, 0, 0, time.UTC),
		EndAt:   time.Date(now.Year(), now.Month(), now.Day(), 10, 30, 0, 0, time.UTC),
	}
	expect = true
	got = IsConflict(r1, r2)
	assert.Equal(t, expect, got)

	// case 5
	// r1       |--------|
	// r2  |--|
	r1 = TimeRange{
		StartAt: time.Date(now.Year(), now.Month(), now.Day(), 9, 0, 0, 0, time.UTC),
		EndAt:   time.Date(now.Year(), now.Month(), now.Day(), 10, 0, 0, 0, time.UTC),
	}

	r2 = TimeRange{
		StartAt: time.Date(now.Year(), now.Month(), now.Day(), 8, 10, 0, 0, time.UTC),
		EndAt:   time.Date(now.Year(), now.Month(), now.Day(), 8, 30, 0, 0, time.UTC),
	}
	expect = false
	got = IsConflict(r1, r2)
	assert.Equal(t, expect, got)

	// case 6
	// r1       |--------|
	// r2                  |---|
	r1 = TimeRange{
		StartAt: time.Date(now.Year(), now.Month(), now.Day(), 9, 0, 0, 0, time.UTC),
		EndAt:   time.Date(now.Year(), now.Month(), now.Day(), 10, 0, 0, 0, time.UTC),
	}

	r2 = TimeRange{
		StartAt: time.Date(now.Year(), now.Month(), now.Day(), 10, 10, 0, 0, time.UTC),
		EndAt:   time.Date(now.Year(), now.Month(), now.Day(), 10, 30, 0, 0, time.UTC),
	}
	expect = false
	got = IsConflict(r1, r2)
	assert.Equal(t, expect, got)
}

func TestIsConflictOfMultiple(t *testing.T) {
	now := time.Now()
	// case 1
	// r1       |--------|
	// r2                  |---|
	// r3                     |----|
	r1 := TimeRange{
		StartAt: time.Date(now.Year(), now.Month(), now.Day(), 9, 0, 0, 0, time.UTC),
		EndAt:   time.Date(now.Year(), now.Month(), now.Day(), 10, 0, 0, 0, time.UTC),
	}

	r2 := TimeRange{
		StartAt: time.Date(now.Year(), now.Month(), now.Day(), 10, 10, 0, 0, time.UTC),
		EndAt:   time.Date(now.Year(), now.Month(), now.Day(), 10, 30, 0, 0, time.UTC),
	}

	r3 := TimeRange{
		StartAt: time.Date(now.Year(), now.Month(), now.Day(), 10, 20, 0, 0, time.UTC),
		EndAt:   time.Date(now.Year(), now.Month(), now.Day(), 11, 30, 0, 0, time.UTC),
	}

	tr := []TimeRange{r1, r2, r3}
	expect := true
	got := IsConflictOfMultiple(tr)
	assert.Equal(t, expect, got)
}
