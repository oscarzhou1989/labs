package overlap

import (
	"sort"
	"time"
)

type TimeRange struct {
	StartAt time.Time
	EndAt   time.Time
}

func IsConflict(r1, r2 TimeRange) bool {
	if r1.EndAt.Before(r2.StartAt) || r1.StartAt.After(r2.EndAt) {
		return false
	}
	return true
}

// Multiple time ranges check
type TimeRangeArray []TimeRange

func (e TimeRangeArray) Len() int {
	return len(e)
}

func (e TimeRangeArray) Swap(i, j int) {
	e[i], e[j] = e[j], e[i]
}

func (e TimeRangeArray) Less(i, j int) bool {
	return e[i].StartAt.After(e[j].StartAt)
}

func IsConflictOfMultiple(tr []TimeRange) bool {

	sort.Sort(TimeRangeArray(tr))

	tr1 := tr[0]
	for i := 1; i < len(tr); i++ {
		tr2 := tr[i]

		if IsConflict(tr1, tr2) {
			return true
		}
		tr1 = tr[i]
	}
	return false
}





