package main

import "fmt"

// How do you print the first non-repeated character from a string?
func main() {
	var buckets = make(map[rune]int)

	var input string = "hheeddijllo"

	charArr := []rune(input)

	var repeating, nonRepeating []rune
	for i := 0; i < len(charArr); i++ {
		if _, ok := buckets[charArr[i]]; !ok {
			nonRepeating = append(nonRepeating, charArr[i])
			buckets[charArr[i]] = 1
		} else {
			repeating = append(repeating, charArr[i])
			buckets[charArr[i]]++
			nonRepeating = remove(nonRepeating, charArr[i])
		}
	}

	fmt.Printf("%c", nonRepeating[0])
}

func remove(arr []rune, target rune) []rune {
	var retArr []rune
	for _, v := range arr {
		if v != target {
			retArr = append(retArr, v)
		}
	}
	return retArr
}
