package main

import "fmt"

// How do you print duplicate characters from a string

func main() {
	input := "kkkkieosieh"

	charArr := []rune(input)
	var bucket = make(map[rune]int)

	for _, v := range charArr {
		if _, ok := bucket[v]; !ok {
			bucket[v] = 1
		} else {
			if bucket[v] == 1 {
				fmt.Printf("%c ", v)
				bucket[v]++
			}
		}
	}
}
