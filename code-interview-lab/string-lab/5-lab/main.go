package main

import (
	"fmt"
)

// How do you check if a string contains only digits?

func main() {
	input := "kdsincoa'doscoewksfi3jsdfl"
	fmt.Printf("input: %s\n only digits: %v\n", input, checkOnlyDigits(input))

	input = "0232183198471"
	fmt.Printf("input: %s\n only digits: %v\n", input, checkOnlyDigits(input))

}

func checkOnlyDigits(input string) bool {
	charArr := []rune(input)
	for _, v := range charArr {
		pop := int(v - '0')
		if !(pop >= 0 && pop <= 9) {
			return false
		}
	}
	return true
}
