package main

import (
	"fmt"
	"strings"
)

// How are duplicate nodes removed in an unsorted linked list

func main() {

	linkedList := NewLinkedList()
	linkedList.Add(NewNode("101"))
	linkedList.Add(NewNode("201"))
	linkedList.Add(NewNode("301"))
	linkedList.Add(NewNode("401"))
	linkedList.Add(NewNode("301"))
	linkedList.Add(NewNode("601"))
	linkedList.Add(NewNode("401"))
	linkedList.RemoveDuplicates()
	linkedList.Print()

	linkedList = NewLinkedList()
	linkedList.Add(NewNode("12"))
	linkedList.Add(NewNode("11"))
	linkedList.Add(NewNode("12"))
	linkedList.Add(NewNode("21"))
	linkedList.Add(NewNode("41"))
	linkedList.Add(NewNode("43"))
	linkedList.Add(NewNode("21"))
	linkedList.RemoveDuplicates()
	linkedList.Print()
}

type Node struct {
	data string
	next *Node
}

func NewNode(data string) *Node {
	return &Node{
		data: data,
		next: nil,
	}
}

type LinkedList struct {
	head *Node
}

func NewLinkedList() *LinkedList {
	return new(LinkedList)
}

func (e *LinkedList) Add(node *Node) {
	if e.head == nil {
		e.head = node
	} else {
		curNode := e.head
		for curNode.next != nil {
			curNode = curNode.next
		}
		curNode.next = node
	}
}

func (e *LinkedList) Insert(pos int, node *Node) {
	curNode := e.head
	for i := 0; i < pos; i++ {
		curNode = curNode.next
	}
	node.next = curNode.next
	curNode.next = node
}

func (e *LinkedList) RemoveDuplicates() {
	set := make(map[string]bool)

	curNode := e.head
	var prev *Node = nil
	for curNode != nil {
		if _, ok := set[curNode.data]; !ok {
			set[curNode.data] = true
			prev = curNode
		} else {
			prev.next = curNode.next
			curNode = nil
		}
		curNode = prev.next
	}

}

func (e *LinkedList) Print() {
	curNode := e.head
	var strArr []string
	for curNode.next != nil {
		strArr = append(strArr, curNode.data)
		curNode = curNode.next
	}
	strArr = append(strArr, curNode.data)

	fmt.Printf("%s\n", strings.Join(strArr, "--->"))

}
