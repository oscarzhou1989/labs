package singly

import "fmt"

type LinkedList struct {
	head *Node
}

type Node struct {
	data int
	next *Node
}

func NewNode(data int) *Node {
	return &Node{
		data: data,
		next: nil,
	}
}

func NewLinkedList() *LinkedList {
	linkedList := new(LinkedList)
	return linkedList
}

func (e *LinkedList) Add(data int) {
	node := &Node{
		data: data,
		next: nil,
	}
	if e.head == nil {
		e.head = node
	}

	curNode := e.head
	for curNode.next != nil {
		curNode = curNode.next
	}

	curNode.next = node

}

func (e *LinkedList) AddNode(node *Node) {
	if e.head == nil {
		e.head = node
	} else {
		curNode := e.head
		for curNode.next != nil {
			curNode = curNode.next
		}
		curNode.next = node
	}
}

func (e *LinkedList) Reverse() {
	pointer := e.head

	var prev, cur *Node
	prev = nil
	cur = nil

	for pointer != nil {
		cur = pointer
		pointer = pointer.next

		// reverse the linked list
		cur.next = prev
		prev = cur
		e.head = cur
	}

}

func (e *LinkedList) Print() {
	curNode := e.head
	for curNode.next != nil {
		fmt.Printf("%d-->", curNode.data)
		curNode = curNode.next
	}
	fmt.Printf("%d\n", curNode.data)

}

func (e *LinkedList) Insert(pos int, node Node) {
	if e.head == nil {
		e.head = &node
	} else {
		curNode := e.head
		for pos != 0 {
			pos--
			curNode = curNode.next
		}

		node.next = curNode.next
		curNode.next = &node
	}

}
