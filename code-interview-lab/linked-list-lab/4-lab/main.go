package main

import (
	"fmt"
	"strings"
)

// How do you reverse a singly linked list without recursion

func main() {
	linkedList := NewSinglyLinkedList()
	linkedList.Add("101")
	linkedList.Add("201")
	linkedList.Add("301")
	linkedList.Add("401")
	linkedList.Add("501")
	linkedList.Add("601")

	linkedList.Print()
	linkedList.ReverseIteratively()
	linkedList.Print()
	linkedList.ReverseRecursively(linkedList.head)
	linkedList.Print()

}

type Node struct {
	data string
	next *Node
}

func (e Node) String() string {
	return fmt.Sprintf("%s", e.data)
}

type SinglyLinkedList struct {
	head *Node
}

func NewSinglyLinkedList() *SinglyLinkedList {
	return new(SinglyLinkedList)
}

func (e *SinglyLinkedList) Add(data string) {
	node := &Node{
		data: data,
		next: nil,
	}

	if e.head == nil {
		e.head = node
	} else {
		curNode := e.head
		for curNode.next != nil {
			curNode = curNode.next
		}
		curNode.next = node
	}
}

func (e *SinglyLinkedList) Print() {
	curNode := e.head

	var strArr []string
	for curNode.next != nil {
		strArr = append(strArr, curNode.String())
		curNode = curNode.next
	}
	strArr = append(strArr, curNode.String())

	fmt.Printf("%s\n", strings.Join(strArr, "--->"))
}

// ReverseIteratively
func (e *SinglyLinkedList) ReverseIteratively() {
	ptr := e.head
	var cur, prev *Node = nil, nil

	for ptr != nil {
		cur = ptr
		ptr = ptr.next

		cur.next = prev
		prev = cur
		e.head = cur
	}
}

// ReverseRecursively
func (e *SinglyLinkedList) ReverseRecursively(node *Node) *Node {
	if node.next == nil {
		e.head = node
		return node
	}
	newNode := e.ReverseRecursively(node.next)
	newNode.next = node
	node.next = nil
	return node
}
