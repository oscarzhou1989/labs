package main

import "labs/code-interview-lab/linked-list-lab/singly-lab"

// How do you reverse a linked list
func main() {
	linkedList := singly.NewLinkedList()

	linkedList.AddNode(singly.NewNode(101))
	linkedList.AddNode(singly.NewNode(201))
	linkedList.AddNode(singly.NewNode(301))
	linkedList.AddNode(singly.NewNode(401))
	linkedList.AddNode(singly.NewNode(501))

	linkedList.Print()
	linkedList.Reverse()
	linkedList.Print()
}
