package main

import "fmt"

// How do you find the middle element of a singly linked list in one pass

func main() {

	linkedList := NewLinkedList()
	linkedList.Add(1)
	linkedList.Add(2)
	linkedList.Add(3)
	linkedList.Add(4)
	linkedList.Add(5)
	linkedList.Print()

	firstNode := linkedList.Head()
	secondNode := linkedList.Head()
	count := 0

	for firstNode.next != nil {
		count++
		if count%2 == 0 {
			secondNode = secondNode.next
		}

		firstNode = firstNode.next
	}
	count++

	fmt.Println("length = ", count)
	fmt.Println("middle of linked list: ", secondNode.data)
}

type LinkedList struct {
	head *Node
}

type Node struct {
	data int
	next *Node
}

func NewLinkedList() *LinkedList {
	return new(LinkedList)
}

func (e *LinkedList) Head() *Node {
	return e.head
}

func (e *LinkedList) Add(data int) {
	node := &Node{
		data: data,
		next: nil,
	}

	if e.head == nil {
		(*e).head = node
	} else {
		curNode := e.head
		for curNode.next != nil {
			curNode = curNode.next
		}

		curNode.next = node
	}
}

func (e *LinkedList) Print() {
	curNode := e.head

	for curNode.next != nil {
		fmt.Printf("%d ", curNode.data)
		curNode = curNode.next
	}
	fmt.Printf("%d ", curNode.data)
}
