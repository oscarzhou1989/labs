package main

import (
	"fmt"
	"labs/code-interview-lab/linked-list-lab/circularly-lab"
	"labs/code-interview-lab/linked-list-lab/singly-lab"
)

// How do you check if a given linked list contains a cycle?
// How do you find the starting node of the cycle

func main() {

	linkedList := createCircularLinkedList()
	linkedList.Print()
	// fmt.Printf("%s", linkedList)

	FindStartingNodeOfLoop(linkedList)
}

func FindStartingNodeOfLoop(linkedList *circularly.CircularlyLinkedList) {
	fmt.Println("starting node is ", *linkedList.FindStartingNode())
}

func createSinglyLinkedList() *singly.LinkedList {
	lst := singly.NewLinkedList()
	lst.Add(101)
	lst.Add(201)
	lst.Add(301)
	lst.Add(401)
	return lst
}

func createCircularLinkedList() *circularly.CircularlyLinkedList {
	lst := circularly.NewCircularlyLinkedList()
	lst.AddNode(&circularly.Node{Data: "101", Next: nil})
	lst.AddNode(&circularly.Node{Data: "201", Next: nil})
	lst.AddNode(&circularly.Node{Data: "301", Next: nil})
	node := lst.TailNode()
	lst.AddNode(&circularly.Node{Data: "401", Next: nil})
	lst.AddNode(&circularly.Node{Data: "501", Next: nil})
	lst.AddNode(node)
	return lst

}
