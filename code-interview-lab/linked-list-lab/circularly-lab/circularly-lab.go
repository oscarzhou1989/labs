package circularly

import (
	"fmt"
	"strings"
)

type Node struct {
	Data string
	Next *Node
}

func (e Node) String() string {
	return fmt.Sprintf("%s ", e.Data)
}

type CircularlyLinkedList struct {
	head *Node
}

func NewCircularlyLinkedList() *CircularlyLinkedList {
	return new(CircularlyLinkedList)
}

func (e *CircularlyLinkedList) Add(data string) {
	node := &Node{
		Data: data,
		Next: nil,
	}
	if e.head == nil {
		e.head = node
	} else {
		curNode := e.head
		for curNode.Next != nil {
			curNode = curNode.Next
		}
		curNode.Next = node
	}
}

// IsCyclic How do you check if a given linked list contains a cycle?
func (e *CircularlyLinkedList) IsCyclic() bool {
	fast := e.head
	slow := e.head
	for fast != nil && fast.Next != nil {
		slow = slow.Next
		fast = fast.Next.Next
		if fast == slow {
			return true
		}

	}
	return false
}

func (e *CircularlyLinkedList) AddNode(node *Node) {
	if e.head == nil {
		e.head = node
	} else {
		curNode := e.head
		for curNode.Next != nil {
			curNode = curNode.Next
		}
		curNode.Next = node
	}
}

func (e *CircularlyLinkedList) TailNode() *Node {
	curNode := e.head
	for curNode.Next != nil {
		curNode = curNode.Next
	}
	return curNode
}

func (e CircularlyLinkedList) String() string {
	curNode := e.head

	var s []string
	for curNode.Next != nil {
		s = append(s, curNode.String())
		curNode = curNode.Next
	}
	s = append(s, curNode.String())
	return strings.Join(s, "-->")
}

func (e *CircularlyLinkedList) Print() {
	if e.IsCyclic() {
		fmt.Println("yes. it is a cyclic linked list")

	} else {
		fmt.Println("no. it is not a cyclic linked list")
	}
}

// FindStartingNode How do you find the starting node of the cycle
func (e *CircularlyLinkedList) FindStartingNode() *Node {
	fastNode := e.head
	slowNode := e.head

	for fastNode != nil && fastNode.Next != nil {
		slowNode = slowNode.Next
		fastNode = fastNode.Next.Next

		if fastNode == slowNode {
			break
		}
	}

	if fastNode != slowNode {
		fmt.Println("no cyclic linked list")
	}

	slowNode = e.head
	for slowNode != fastNode {
		slowNode = slowNode.Next
		fastNode = fastNode.Next
	}
	return slowNode
}
