package tree

import "testing"

func TestTree(t *testing.T) {
	t1 := NewTree("D", nil, nil)
	t2 := NewTree("E", nil, nil)
	t3 := NewTree("B", t1, t2)

	t4 := NewTree("F", nil, nil)
	t5 := NewTree("G", nil, nil)
	t6 := NewTree("C", t4, t5)

	root := NewTree("A", t3, t6)

	// root.TraverseByPreOrder()

	// root.TraverseInOrder()

	root.TraversePostOrder()
	t.Error()

}
