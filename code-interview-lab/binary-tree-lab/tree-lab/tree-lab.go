package tree

import "fmt"

type Tree struct {
	data  string
	left  *Tree
	right *Tree
}

func NewTree(data string, left, right *Tree) *Tree {
	return &Tree{
		data:  data,
		left:  left,
		right: right,
	}
}

func (e *Tree) TraverseByPreOrder() {
	if e != nil {
		fmt.Printf("%s ", e.data)
		e.left.TraverseByPreOrder()
		e.right.TraverseByPreOrder()
	}
}

func (e *Tree) TraverseInOrder() {
	if e.left != nil {
		e.left.TraverseInOrder()
		fmt.Printf("%s ", e.data)
		e.right.TraverseInOrder()
	} else {
		fmt.Printf("%s ", e.data)
	}
}

func (e *Tree) TraversePostOrder() {
	if e.left != nil {
		e.left.TraversePostOrder()
		e.right.TraversePostOrder()
		fmt.Printf("%s ", e.data)
	} else {
		fmt.Printf("%s ", e.data)
	}
}
