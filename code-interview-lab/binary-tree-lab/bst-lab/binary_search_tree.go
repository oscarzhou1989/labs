package bst

import "fmt"

// BST known as binary search tree
// A binary search tree is a binary tree where the value of
// a left child is less than or equal to the parent node and
// value of the right child is greater than or equal to the
// parent node.

type BST struct {
	root *Node
}

func (e *BST) GetRoot() *Node {
	return e.root
}

func (e *BST) IsEmpty() bool {
	return e.root == nil
}

func (e *BST) Size() int {
	return e.root.traversePreOrder()
}

func (e *BST) Clear() {
	e.root = nil
}

func (e *BST) Insert(data int) {
	node := &Node{
		data:  data,
		left:  nil,
		right: nil,
	}

	if e.root == nil {
		e.root = node
	} else {
		e.root.Insert(node)
	}
}

func (e *BST) Print() {
	e.root.traversePreOrder()

}

type Node struct {
	data  int
	left  *Node
	right *Node
}

func (e *Node) traversePreOrder() int {
	count := 0
	if e != nil {
		count++
		fmt.Printf("%d ", e.data)
		count += e.left.traversePreOrder()
		count += e.right.traversePreOrder()
	}
	return count
}

func (e *Node) Insert(node *Node) {
	if node.data < e.data {
		if e.left != nil {
			e.left.Insert(node)
		} else {
			e.left = node
		}
	} else {
		if e.right != nil {
			e.right.Insert(node)
		} else {
			e.right = node
		}
	}
}
