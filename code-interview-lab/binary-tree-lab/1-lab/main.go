package main

import (
	"fmt"
	"labs/code-interview-lab/binary-tree-lab/bst-lab"
)

// How is a binary search tree implemented?

func main() {
	bst := new(bst.BST)
	bst.Insert(50)
	bst.Insert(30)
	bst.Insert(20)
	bst.Insert(40)
	bst.Insert(70)
	bst.Insert(60)
	bst.Insert(80)

	fmt.Printf("\ntotal=%d\n", bst.Size())
}
