package main

import "fmt"

// How is a bubble sort algorithm implemented

func main() {

	input := []int{5, 1, 6, 2, 4, 3}
	fmt.Println(BubbleSort(input))
}

func BubbleSort(arr []int) []int {
	for i := 0; i < len(arr)-1; i++ {
		for j := i + 1; j < len(arr); j++ {
			if arr[i] > arr[j] {
				arr[i], arr[j] = arr[j], arr[i]
			}
		}
	}
	return arr
}
