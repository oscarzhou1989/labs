package main

import (
	"fmt"
)

// How is an iterative quicksort algorithm implemented?

func main() {
	input := []int{34, 32, 43, 12, 11, 32, 22, 21, 32}
	fmt.Println(QuickSortIterative(input))
	// fmt.Println(QuickSortRecursive(input))
}

func QuickSortIterative(arr []int) []int {
	stk := NewStack()
	stk.Push(0)
	stk.Push(len(arr) - 1)

	for !stk.IsNil() {
		high := stk.Pop()
		low := stk.Pop()

		fmt.Printf("low=%d,high=%d\n", low, high)
		if high-low < 2 {
			continue
		}

		pivot := partition(&arr, low, high)
		fmt.Println("pivot=", pivot)
		fmt.Println("arr=", arr)

		stk.Push(pivot + 1)
		stk.Push(high)

		stk.Push(low)
		stk.Push(pivot - 1)

	}
	return arr
}

func partition(arr *[]int, start, end int) int {
	low := start
	high := end - 1
	pivot := (*arr)[end]

	for low < high {
		// fmt.Printf("arr[low]=%d,pivot=%d, arr[high]=%d", (*arr)[low], pivot, (*arr)[high])
		if (*arr)[low] < pivot {
			low++
		} else if (*arr)[high] >= pivot {
			high--
		} else {
			(*arr)[low], (*arr)[high] = (*arr)[high], (*arr)[low]
		}
	}

	idx := high
	if (*arr)[high] < pivot {
		idx++
	}
	(*arr)[idx], (*arr)[end] = (*arr)[end], (*arr)[idx]
	return idx
}

func QuickSortRecursive(arr []int) []int {
	if len(arr) < 2 {
		return arr
	}

	pivot := len(arr) / 2

	var leftArr, rightArr []int
	for i, v := range arr {
		if i != pivot {
			if v < arr[pivot] {
				leftArr = append(leftArr, v)
			} else {
				rightArr = append(rightArr, v)
			}
		}
	}

	var retArr []int
	retArr = append(retArr, QuickSortRecursive(leftArr)...)
	retArr = append(retArr, arr[pivot])
	retArr = append(retArr, QuickSortRecursive(rightArr)...)
	return retArr
}

type Stack struct {
	base *Node
	top  *Node
}

type Node struct {
	data int
	next *Node
}

func NewStack() *Stack {
	return new(Stack)
}

func (e *Stack) Push(data int) {
	node := &Node{
		data: data,
		next: nil,
	}

	if e.base == nil {
		e.base = node
		e.top = node
	} else {
		e.top.next = node
		e.top = e.top.next
	}
}

func (e *Stack) Pop() int {
	if e.base == nil {
		return -1
	}

	if e.base == e.top {
		ret := e.top.data
		e.top = nil
		e.base = nil
		return ret
	}

	curNode := e.base
	for curNode.next != e.top {
		curNode = curNode.next
	}

	ret := e.top.data
	e.top = curNode
	return ret
}

func (e *Stack) IsNil() bool {
	if e.base == nil {
		return true
	}
	return false
}
