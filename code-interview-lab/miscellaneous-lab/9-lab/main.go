package main

import "fmt"

// How do you check if two rectangles overlap with each other?

func main() {
	lt1 := Point{0, 10}
	rb1 := Point{10, 0}
	lt2 := Point{5, 5}
	rb2 := Point{15, 0}

	r1 := Rectangle{Lt: lt1, Rb: rb1}
	r2 := Rectangle{Lt: lt2, Rb: rb2}

	fmt.Println(isOverlapping(r1, r2))

	lt1 = Point{0, 5}
	rb1 = Point{5, 0}
	lt2 = Point{15, 5}
	rb2 = Point{10, 0}

	r1 = Rectangle{Lt: lt1, Rb: rb1}
	r2 = Rectangle{Lt: lt2, Rb: rb2}

	fmt.Println(isOverlapping(r1, r2))
}

type Point struct {
	X, Y int
}

type Rectangle struct {
	Lt Point
	Rb Point
}

func isOverlapping(r1, r2 Rectangle) bool {
	if r1.Rb.X < r2.Lt.X ||
		r1.Lt.X > r2.Rb.X ||
		r1.Rb.Y > r2.Lt.Y ||
		r1.Lt.Y < r2.Rb.Y {
		return false
	}

	return true
}
