package main

import (
	"fmt"
	"sort"
)

// How do you implement a counting sort algorithm?

func main() {
	input := []int{60, 40, 30, 20, 10, 40, 30, 60, 60, 20, 40, 30, 40}
	fmt.Println(input)

	fmt.Println(CountingSort(input))
}

func CountingSort(arr []int) []int {
	countArr := make(map[int]int)
	for _, key := range arr {
		if _, ok := countArr[key]; !ok {
			countArr[key] = 1
		} else {
			countArr[key]++
		}
	}

	var keys []int
	for key := range countArr {
		keys = append(keys, key)
	}
	sort.Sort(sort.IntSlice(keys))

	k := 0
	newArr := make([]int, len(arr))
	for _, key := range keys {
		for countArr[key] > 0 {
			newArr[k] = key
			countArr[key]--
			k++
		}
	}

	return newArr
}
