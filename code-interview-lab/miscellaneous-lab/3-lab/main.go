package main

import "fmt"

// How do you implement an insertion sort algorithm

func main() {
	input := []int{74, 87, 27, 6, 25, 94, 53, 91, 15}
	fmt.Println(InsertionSort(input))
}

func InsertionSort(arr []int) []int {
	retArray := make([]int, len(arr))

	retArray[0] = arr[0]
	for i := 1; i < len(arr); i++ {
		length := i + 1
		retArray[i] = arr[i]
		cur := arr[i]
		for j := length - 2; j >= 0; j-- {
			if cur < retArray[j] {
				retArray[j+1], retArray[j] = retArray[j], retArray[j+1]
			} else {
				break
			}
		}
	}

	return retArray
}
