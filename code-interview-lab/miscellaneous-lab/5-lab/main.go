package main

import "fmt"

// How do you implement a bucket sort algorithm?

// The difference between BucketSort and CountingSort is
// that BucketSort uses a hash function to distribute keys
// while CountingSort creates a buckets for each key
func main() {
	input := []float64{0.78, 0.17, 0.39, 0.26, 0.72, 0.94, 0.21, 0.12, 0.23, 0.68}
	fmt.Println(BucketSort(input))
}

func BucketSort(arr []float64) []float64 {
	bucket := make(map[int][]float64)

	for _, v := range arr {
		bucket[hash(v)] = append(bucket[hash(v)], v)
	}

	for key, value := range bucket {
		bucket[key] = InsertionSort(value)
	}

	var tempArr []float64
	for i := 0; i < len(arr); i++ {
		if v, ok := bucket[i]; ok {
			tempArr = append(tempArr, v...)
		}
	}

	return tempArr
}

func hash(x float64) int {
	return int(x * 10)
}

func InsertionSort(arr []float64) []float64 {
	tempArr := make([]float64, len(arr))

	tempArr[0] = arr[0]
	for i := 1; i < len(arr); i++ {
		tempArr[i] = arr[i]
		length := i + 1

		for j := length - 2; j >= 0; j-- {
			if tempArr[j+1] < tempArr[j] {
				tempArr[j+1], tempArr[j] = tempArr[j], tempArr[j+1]
			} else {
				break
			}
		}
	}
	return tempArr
}
