package main

import (
	"fmt"
	"sort"
)

// How is a radix sort algorithm implemented?

func main() {
	input := []int{170, 45, 75, 90, 802, 24, 2, 66}
	fmt.Println(input)
	fmt.Println(RadixSort(&input))
}

func getMax(arr []int) int {
	max := 0
	for _, v := range arr {
		if max < v {
			max = v
		}
	}
	return max
}

func RadixSort(arr *[]int) []int {
	max := getMax(*arr)

	for exp := 1; max/exp > 0; exp *= 10 {
		bucketSortArray(arr, exp)
	}
	return *arr
}

func bucketSortArray(arr *[]int, exp int) {
	bucket := make(map[int][]int)

	for _, v := range *arr {
		x := (v / exp) % 10
		bucket[x] = append(bucket[x], v)
	}

	var keys []int
	for key := range bucket {
		keys = append(keys, key)
	}
	sort.Sort(sort.IntSlice(keys))

	var retArray []int
	for _, key := range keys {
		retArray = append(retArray, bucket[key]...)
	}
	(*arr) = retArray
}
