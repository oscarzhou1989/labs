package stack

import "testing"

func TestStack(t *testing.T) {
	stack := NewStack()

	stack.Push(NewNode(24))
	stack.Push(NewNode(44))
	stack.Push(NewNode(55))
	stack.Push(NewNode(56))
	stack.Push(NewNode(67))
	stack.Push(NewNode(88))
	stack.Pop()
	stack.Pop()

	stack.Print()
	t.Error()
}
