package stack

import "fmt"

type Stack struct {
	base *Node
	top  *Node
}

func NewStack() *Stack {
	return new(Stack)
}

func (e *Stack) Push(node *Node) {
	if e.base == nil {
		e.base = node
		e.top = node
	} else {
		e.top.next = node
		e.top = node
	}
}

func (e *Stack) Pop() *Node {
	if e.base == nil {
		return nil
	}

	if e.base == e.top {
		popNode := e.top
		e.base = nil
		e.top = nil
		return popNode
	}
	curNode := e.base
	for curNode.next != e.top {
		curNode = curNode.next
	}
	popNode := curNode.next
	e.top = curNode
	return popNode
}

func (e *Stack) IsNil() bool {
	if e.base == nil {
		return true
	}
	return false
}

func (e *Stack) Print() {
	curNode := e.base
	for curNode != e.top {
		fmt.Printf("%d ", curNode.data)
		curNode = curNode.next
	}
	fmt.Printf("%d ", curNode.data)
}

type Node struct {
	data int
	next *Node
}

func NewNode(data int) *Node {
	return &Node{
		data: data,
		next: nil,
	}
}
