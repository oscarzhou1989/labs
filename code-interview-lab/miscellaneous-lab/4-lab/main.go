package main

import "fmt"

// How is a merge sort algorithm implemented?

// Explain the difference between MergeSort and QuickSort
func main() {
	input := []int{80, 50, 30, 10, 90, 60, 0, 70, 40, 20, 5}
	fmt.Println(input)
	MergeSort(&input, 0, len(input)-1)
	fmt.Println(input)
}

func MergeSort(arr *[]int, low, high int) {
	if low < high {
		mid := (low + high) / 2
		MergeSort(arr, low, mid)
		MergeSort(arr, mid+1, high)

		merge(arr, low, mid, high)
	}

}

func merge(arr *[]int, low, mid, high int) {
	length1 := mid - low + 1
	length2 := high - mid

	left := make([]int, length1)
	right := make([]int, length2)

	for i := 0; i < length1; i++ {
		left[i] = (*arr)[low+i]
	}

	for j := 0; j < length2; j++ {
		right[j] = (*arr)[mid+1+j]
	}
	var i, j int = 0, 0
	k := low

	for i < length1 && j < length2 {
		if left[i] <= right[j] {
			(*arr)[k] = left[i]
			i++
		} else {
			(*arr)[k] = right[j]
			j++
		}
		k++
	}

	for i < length1 {
		(*arr)[k] = left[i]
		i++
		k++
	}

	for j < length2 {
		(*arr)[k] = right[j]
		j++
		k++
	}

}
