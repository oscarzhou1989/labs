package bsd

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestBSDSearch(t *testing.T) {
	graph := make(map[string][]string)

	graph["you"] = []string{"alice", "bob", "claire"}
	graph["alice"] = []string{"peggy"}
	graph["bob"] = []string{"peggy", "anuj"}
	graph["claire"] = []string{"thom", "jonny"}
	graph["peggy"] = []string{}
	graph["anuj"] = []string{}
	graph["thom"] = []string{}
	graph["jonny"] = []string{}

	expect := true
	got := BSDSearch(graph)

	assert.Equal(t, expect, got)
}
