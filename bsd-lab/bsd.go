package bsd

import (
	"fmt"
	"labs/queue-lab"
)

func BSDSearch(graph map[string][]string) bool {
	searchQueue := queue.NewQueue()
	for _, v := range graph["you"] {
		searchQueue.Enqueue(v)
	}

	searched := []string{}
	for {
		popItem := searchQueue.Dequeue()
		if popItem != nil {
			item, ok := popItem.(string)
			if ok {
				if !isSearched(item, searched) {

					if isJonny(item) {
						fmt.Println("Jonny is here")
						return true
					}

					searched = append(searched, item)
					for _, v := range graph[item] {
						searchQueue.Enqueue(v)
					}
				}

			}
		} else {
			break
		}
	}

	fmt.Println("Can't find jonny")
	return false
}

func isSearched(target string, searched []string) bool {
	for _, v := range searched {
		if v == target {
			return true
		}
	}
	return false
}

func isJonny(target string) bool {
	if target == "jonny" {
		return true
	}
	return false
}
