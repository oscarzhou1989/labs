package main

import (
	"fmt"
	"io/ioutil"
	"os"

	"github.com/faiface/pixel"
	"github.com/faiface/pixel/pixelgl"
	"github.com/faiface/pixel/text"
	"github.com/golang/freetype/truetype"
	"golang.org/x/image/colornames"
	"golang.org/x/image/font"
)

func run() {
	cfg := pixelgl.WindowConfig{
		Title:  "Pixel Rocks!",
		Bounds: pixel.R(0, 0, 1024, 768),
		VSync:  true,
	}

	win, err := pixelgl.NewWindow(cfg)
	if err != nil {
		panic(err)
	}

	face, err := loadTTF("intuitive.ttf", 52)
	if err != nil {
		panic(err)
	}

	atlas := text.NewAtlas(face, text.ASCII)
	basicTxt := text.New(pixel.V(100, 500), atlas)

	basicTxt.Color = colornames.Red
	fmt.Fprintln(basicTxt, "Hello, pixel!")

	basicTxt.Color = colornames.Green
	fmt.Fprintln(basicTxt, "I support multiple lines!")

	basicTxt.Color = colornames.Blue
	fmt.Fprintf(basicTxt, "And I'm an %d, yay!", 5)
	for !win.Closed() {
		basicTxt.WriteString(win.Typed())
		if win.JustPressed(pixelgl.KeyEnter) {
			basicTxt.WriteRune('\n')
		}

		win.Clear(colornames.Black)
		// basicTxt.Draw(win, pixel.IM.Scaled(basicTxt.Orig, 4))
		// basicTxt.Draw(win, pixel.IM)
		basicTxt.Draw(win, pixel.IM.Moved(win.Bounds().Center().Sub(basicTxt.Bounds().Center())))
		win.Update()
	}
}

func main() {
	pixelgl.Run(run)
}

func loadTTF(path string, size float64) (font.Face, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	bytes, err := ioutil.ReadAll(file)
	if err != nil {
		return nil, err
	}

	font, err := truetype.Parse(bytes)
	if err != nil {
		return nil, err
	}

	return truetype.NewFace(font, &truetype.Options{
		Size:              size,
		GlyphCacheEntries: 1,
	}), nil
}
